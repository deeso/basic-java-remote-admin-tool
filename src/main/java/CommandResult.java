import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class CommandResult {
	Object myValue = null;
	boolean myCallFailed = true;
	CommandResult() {
		myValue = null;
	}
	CommandResult(Object value) {
		myValue = value;
	}
	CommandResult(Object value, boolean failed) {
		myValue = value;
		myCallFailed = failed;
	}
	public byte[] serialize() {
		if (myValue == null){
			return "null".getBytes();
		}
		Class<?> klass = myValue.getClass();
		Method getBytes = null;
		try {
			getBytes = klass.getMethod("getBytes");
		} catch (NoSuchMethodException e) {
		} catch (SecurityException e) {
		}
		if (getBytes != null) {
			try {
				return (byte[]) getBytes.invoke(myValue);
			} catch (IllegalAccessException e) {
			} catch (IllegalArgumentException e) {
			} catch (InvocationTargetException e) {
			}
		}
		return myValue.toString().getBytes();
	}
		
}