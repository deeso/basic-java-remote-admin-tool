import sys, os, socket, struct
from path_var import *
from recoop import recoop_jvm8
from jvm.extract_process import ExtractProc
from jvm.mem_chunks import MemChunk
from jvm.jvm_analysis import JVMAnalysis
from jvm.jvm_systemdictionary import Dictionary
from jvm.jvm_stringtable import StringTable
from jvm.jvm_symboltable import SymbolTable
from jvm.jvm_klassoop import Oop
profile = 'LinuxUbuntu1504-whatx86'
dump_java_process=False
# ps -ef | grep java
# java@java-workx32-00:~/dotcms$ ps -ef | grep java
# java     13678     1  0 11:33 pts/0    00:00:11 java -jar /home/java/dotcms/dotserver/tomcat-8.0.18/webapps/ROOT/java-rat-all-1.0.jar 57800 1000
# java     17133     1 20 11:44 pts/0    00:25:15 /usr/bin/java -Djava.util.logging.config.file=/home/java/dotcms/dotserver/tomcat-8.0.18/conf/logging.properties -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager -XX:+PrintGC -XX:+PrintGCApplicationConcurrentTime -XX:+PrintGCApplicationStoppedTime -XX:+PrintGCDateStamps -XX:+PrintGCTaskTimeStamps -XX:+PrintGCTimeStamps -Xloggc:/home/java/dotcms_gc.txt -Djava.awt.headless=true -Xverify:none -Dfile.encoding=UTF8 -server -Xms2G -Xmx2G -XX:+DisableExplicitGC -XX:+UseSerialGC -javaagent:/home/java/dotcms/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/lib/dot.jamm-0.2.5_2.jar -Ddotserver=dotcms -Djava.endorsed.dirs=/home/java/dotcms/dotserver/tomcat-8.0.18/endorsed -classpath /home/java/dotcms/dotserver/tomcat-8.0.18/bin/bootstrap.jar:/home/java/dotcms/dotserver/tomcat-8.0.18/bin/tomcat-juli.jar -Dcatalina.base=/home/java/dotcms/dotserver/tomcat-8.0.18 -Dcatalina.home=/home/java/dotcms/dotserver/tomcat-8.0.18 -Djava.io.tmpdir=/home/java/dotcms/dotserver/tomcat-8.0.18/temp org.apache.catalina.startup.Bootstrap start

#vol.py --profile LinuxUbuntu1504-whatx86 -f /home/dso/dotcms-exp-load-plugin/exp-t0/java-workx32-00.dump linux_pslist
# dso@master-chief:/research_data/code/git/jvm_analysis$ vol.py --profile LinuxUbuntu1504-whatx86 -f /home/dso/dotcms-exp-load-plugin/exp-t0/java-workx32-00.dump linux_pslist | grep java
# Volatility Foundation Volatility Framework 2.4
# 0xeb8b4980 java                 13678           1000            1000   0x249c8000 0
# 0xd7acb720 java                 17133           1000            1000   0x2d0df000 0
# 0xea4f24c0 java                 18149           1000            1000   0x134e9000 0
pids = [
13678,
17133,
18149,
]
exp_t0_dump_file = "/home/dso/dotcms-exp-load-plugin/exp-t0/java-workx32-00.dump"
exp_t0_13678_dump_dir = "/home/dso/dotcms-exp-load-plugin/exp-t0/13678/"
exp_t0_17133_dump_dir = "/home/dso/dotcms-exp-load-plugin/exp-t0/17133/"
exp_t0_18149_dump_dir = "/home/dso/dotcms-exp-load-plugin/exp-t0/18149/"

# dso@master-chief:/research_data/code/git/jvm_analysis$ vol.py --profile LinuxUbuntu1504-whatx86 -f /home/dso/dotcms-exp-load-plugin/exp-t680/java-workx32-00.dump linux_pslist | grep java
# Volatility Foundation Volatility Framework 2.4
# 0xeb8b4980 java                 13678           1000            1000   0x249c8000 0
# 0xd7acb720 java                 17133           1000            1000   0x2d0df000 0
# 0xea4f24c0 java                 18149           1000            1000   0x134e9000 0


exp_t680_dump_file = "/home/dso/dotcms-exp-load-plugin/exp-t680/java-workx32-00.dump"
exp_t680_13678_dump_dir = "/home/dso/dotcms-exp-load-plugin/exp-t680/13678/"
exp_t680_17133_dump_dir = "/home/dso/dotcms-exp-load-plugin/exp-t680/17133/"
exp_t680_18149_dump_dir = "/home/dso/dotcms-exp-load-plugin/exp-t680/18149/"

recoop_1 = recoop_jvm8.RecOOPJVM8(path_to_dumps=exp_t0_17133_dump_dir, path_to_mem=exp_t0_dump_file,jvm_start_addr=None
, jvm_ver="8u60", dump_java_process=False, pid=17133)
recoop_1.next_step(profile=profile)
recoop_1.next_step(profile=profile)
recoop_1.next_step(profile=profile)
recoop_1.next_step(profile=profile)


pid = pids[0]
recoop_0 = recoop_jvm8.RecOOPJVM8(path_to_dumps=exp_t0_13678_dump_dir, path_to_mem=exp_t0_dump_file,jvm_start_addr=None
, jvm_ver="8u60", dump_java_process=True, pid=13678)
recoop_0.next_step(profile=profile)
recoop_0.next_step(profile=profile)
recoop_0.next_step(profile=profile)
recoop_0.next_step(profile=profile)
recoop_0.next_step(profile=profile)

# recoop_1 = recoop_jvm8.RecOOPJVM8(path_to_dumps=exp_t0_17133_dump_dir, path_to_mem=exp_t0_dump_file,jvm_start_addr=None
# , jvm_ver="8u60", dump_java_process=True, pid=17133)
recoop_1.next_step(profile=profile)
recoop_1.next_step(profile=profile)
recoop_1.next_step(profile=profile)


recoop_2 = recoop_jvm8.RecOOPJVM8(path_to_dumps=exp_t680_17133_dump_dir, path_to_mem=exp_t680_dump_file,jvm_start_addr=None
, jvm_ver="8u60", dump_java_process=False, pid=17133)
recoop_2.next_step(profile=profile)
recoop_2.next_step(profile=profile)
recoop_2.next_step(profile=profile)
recoop_2.next_step(profile=profile)
recoop_2.next_step(profile=profile)


k = recoop_jva5.find_all_loaded_klass_oops('java/lang/UNIXProcess')
[i for i in jva5.loaded_classes_by_name if i.find("Process") > -1]
[i for i in recoop_jva5.jva.loaded_classes_by_name if i.find("Process") > -1]
[i for i in jva5.loaded_classes_by_name if i.find("Builder") > -1]
[i for i in recoop_jva5.jva.loaded_classes_by_name if i.find("Builder") > -1]
g = le_set | be_set
k = recoop_jva5.find_all_loaded_klass_oops('java/lang/UNIXProcess')
l = recoop_jva5.find_all_loaded_klass_oops('java/lang/ProcessBuilder')