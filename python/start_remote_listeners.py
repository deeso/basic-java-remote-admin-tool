import socket, struct, base64, binascii, threading, time, select
from socket import  error
#from basic_java_remote_console import *

sock_listener_base = 48000

class SocketListenerServer(threading.Thread):
    def __init__(self, socket_listener, accept_one_client=True):
        threading.Thread.__init__(self)
        self.socket_listener = socket_listener
        self.keep_running = False
        self.one_client = accept_one_client

    def run(self):
        print ("Starting the server listener")
        self.keep_running = True
        while self.keep_running:
            c, a = self.socket_listener.sock.accept()
            sch = SocketClientHandler(c, self.socket_listener, 
                num_sends = self.socket_listener.num_sends, 
                size_sends = self.socket_listener.size_sends, 
                send_spacing = self.socket_listener.send_spacing)
            self.socket_listener.clients.append(sch)
            sch.start()
            if self.one_client:
                break


class SocketClientHandler(threading.Thread):
    def __init__(self, client_sock, socket_listener, num_sends = 100, size_sends=1024, send_spacing=1):
        threading.Thread.__init__(self)
        self.sock = client_sock
        self.sock.setblocking(0)
        self.listener = socket_listener
        self.num_sends = num_sends
        self.size_sends = size_sends
        self.send_spacing = send_spacing

    def run(self):
        host, port = self.sock.getpeername()
        print ("Starting Send/Recieve to: %s:%d"%(host,port))

        while self.num_sends >= 0:
            data = self.listener.get_data(self.size_sends)
            print ("Sending 0x%x bytes to %s:%d"%(len(data), host, port))
            self.sock.send(data)
            if self.send_spacing > 0:
                print ("Sleeping for %ds"%(self.send_spacing))
                time.sleep(self.send_spacing)
            print ("Checking for data")
            rdata = self.perform_recv()
            print ("Recv'ing 0x%x bytes from %s:%d"%(len(rdata), host, port))
            self.num_sends += -1
        print ("Completed Send/Recieve to: %s:%d"%(host,port))

    def perform_recv(self):
        inputs = [self.sock]
        outputs = []
        result = None
        readable, writable, exceptional = select.select(inputs, outputs, inputs)
        if len(readable) > 0:
            print ("Data is available")
            data = self.sock.recv(1024)
            result = data
            print ("Data: "+result)
            while len(data) > 0:
                try:
                    data = self.sock.recv(1024)
                    result = result + data
                except socket.error as err:
                    print("HAD ERROR")
                    break

        return result

class SocketListener(object):
    def __init__(self, sock_id=None, data_size=8192, host='127.0.0.1', data=None,
                       num_sends = 100, size_sends=1024, send_spacing=1):
        global sock_listener_base
        self.sock_id = sock_id
        self.host = host
        if sock_id is None:
            self.sock_id = sock_listener_base
            sock_listener_base += 1
        self.data = data
        self.num_sends = num_sends
        self.size_sends = size_sends
        self.send_spacing = send_spacing
        self.clients = []
        self.got_data_times = 0


    def get_data(self, sz=0):
        if self.data is None:
            data = "s3cr3t_d4t3_%d-%08x"%(self.sock_id, self.got_data_times)
            data = (data*sz)[:sz]
            self.got_data_times += 1
            return data
        if self.size_sends == 0:
            return (self.data*1024)[:1024]
        return (self.data*self.size_sends)[:1024]

    def start(self):
        self.sock = socket.socket()
        self.sock.bind((self.host, self.sock_id))
        self.sock.listen(10)
        self.cur_listener = SocketListenerServer(self)
        self.cur_listener.start()

#rat_host = "java-workx32-00"
#rat_ip = socket.gethostbyname('java-workx32-00.internal.thecoverofnight.com')
#num_servers = 1000

# start 10 sockets locally
num_listeners = 100
master_chief='10.18.120.18'
listeners = [SocketListener(sock_id=sock_listener_base+i, host=master_chief, num_sends=20, size_sends=4096) for i in xrange(0, 10)]
for l in listeners:
    l.start()

