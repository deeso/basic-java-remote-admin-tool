import sys, os, socket, struct
from path_var import *
from recoop import recoop_jvm8
from jvm.extract_process import ExtractProc
from jvm.mem_chunks import MemChunk
from jvm.jvm_analysis import JVMAnalysis
from jvm.jvm_systemdictionary import Dictionary
from jvm.jvm_stringtable import StringTable
from jvm.jvm_symboltable import SymbolTable
from jvm.jvm_klassoop import Oop
from recoop.recoop_jvm8 import time_str
from jvm.jvm_analysis import *

profile = 'LinuxUbuntu1504-whatx86'
dump_java_process=False
pid = 1222
from recoop.recoop_jvm8 import time_str
from jvm.jvm_analysis import *

def log_no_recoop(msg):
    print ("[%s]: %s"%(time_str(), msg))

def analyse_block_structure(blocks):
    valid_addrs = set()
    addr_occurence = {}
    addr_uses = {}
    val_occurrence = {}
    val_uses = {}
    # accumulate addrs
    baddrs = []
    for baddr, block_data in blocks.items():
        offset = block_data['offset']
        word_sz = block_data['word_sz']
        block_sz = len(block_data['dwords'])
        valid_addrs |= set([baddr+i+offset for i in xrange(0, block_sz, word_sz)])
    
    for baddr, block_data in blocks.items():
        _blocks = block_data['dwords']
        value = block_data['val']
        offset = block_data['offset']
        word_sz = block_data['word_sz']
        pos = 0
        for val in _blocks:
            addr = 4*pos + baddr + offset
            if val in valid_addrs:
                if not val in addr_occurence:
                    addr_occurence[val] = {'tot':0}
                    addr_uses[val] = []
                if not pos in addr_occurence[val]:
                    addr_occurence[val][pos] = 0
                addr_occurence[val]['tot'] += 1
                addr_occurence[val][pos] += 1
                addr_uses[val].append((baddr, addr))
            if not val in val_occurrence:
                val_occurrence[val] = {'tot':0}
                val_uses[val] = []
            if not pos in val_occurrence[val]:
                val_occurrence[val][pos] = 0
            val_occurrence[val]['tot'] += 1
            val_occurrence[val][pos] += 1
            val_uses[val].append((baddr, addr))
            pos += 1        

    res_dict = {}
    res_dict['addr_uses'] = addr_uses
    res_dict['addr_occurrence'] = addr_occurence
    res_dict['val_uses'] = val_uses
    res_dict['val_occurrence'] = val_occurrence
    return res_dict


recoop_1 = recoop_jvm8.RecOOPJVM8(                                                                                                                   
    path_to_dumps="/research_data/analysis-rat-events/success-step-by-step/t30/dumps/", 
    path_to_mem="/research_data/analysis-rat-events/success-step-by-step/t30/java-case-study-none-t30.bin", 
    jvm_start_addr=None, jvm_ver="8u60", dump_java_process=dump_java_process, pid=pid)

recoop_1.next_step(profile=profile)
recoop_1.next_step(profile=profile)
recoop_1.next_step(profile=profile)

# vframes = recoop_1.scan_for_java_vframes()
#cwrappers = recoop_1.scan_for_java_call_wrappers()


oop_thread_addrs = set([i for i in recoop_1.thread_oop_mapping_jthread ])
thread_addrs = set([i for i in recoop_1.jthread_start_mapping_thread_oop.keys() if i > 0])
cpc_addrs = set([i.addr for i in recoop_1.jva.known_metas.values() if i._name == 'ConstantPoolCache'])

methods_addrs = set([i.addr for i in recoop_1.jva.known_metas.values() if i._name == 'Method'])
addr_klasses = recoop_1.jva.loaded_classes_by_addr.items()

klasses = [v for k,v in addr_klasses\
                   if str(v).find('Command') == 0 or\
                      str(v).find('Loader') == 0 or\
                      str(v).find('ClientCommand') == 0]

methods = [recoop_1.jva.get_method(i) for i in methods_addrs\
                   if str(recoop_1.jva.get_method(i).klass_holder_value.name_value).find('Command') == 0 or\
                      str(recoop_1.jva.get_method(i).klass_holder_value.name_value).find('Loader') == 0 or\
                      str(recoop_1.jva.get_method(i).klass_holder_value.name_value).find('ClientCommand') == 0]

method_symbols = set([str(m.name()) for m in methods])
klass_symbols = set([str(k) for k in klasses])
symbols = [sym for sym in recoop_1.jva.symboltable_values.values() if str(sym) in method_symbols]

methods_addrs = [i.addr for i in methods]
klass_addrs = [i.addr for i in klasses]
sym_addrs = [sym.addr for sym in symbols]

_, method_addr_uses = recoop_1.jva.scan_pages_for_dword_values(methods_addrs, in_parallel=True)

find_range = recoop_1.jva.find_range
no_int_ranges = dict([(find_range(i).start, find_range(i)) for i in methods_addrs])
int_ranges = [find_range(i) for i in method_addr_uses \
                if not find_range(i).start in no_int_ranges]


_, thread_addr_uses = recoop_1.jva.scan_pages_for_dword_values(thread_addrs, in_parallel=True)
_, cpc_addr_uses    = recoop_1.jva.scan_pages_for_dword_values(cpc_addrs, in_parallel=True)
_, klass_addr_uses  = recoop_1.jva.scan_pages_for_dword_values(klass_addrs, in_parallel=True)
_, sym_addr_uses    = recoop_1.jva.scan_pages_for_dword_values(sym_addrs, in_parallel=True)
_, oop_thread_addr_uses = recoop_1.jva.scan_pages_for_dword_values(oop_thread_addrs, in_parallel=True)
#klass_ranges = get_pot_intersecting_scan_ranges(recoop_1, klass_addrs, klass_addr_uses, sym_addr_uses)
# bci_ranges_constraint = create_bci_ranges(methods)
# bci_ranges = get_pot_bci_ranges(recoop_1, int_method_addrs, int_method_addr_uses, cpc_addr_uses)
# _, bic_addr_uses = par_scan_page_for_bci_addrs(bci_ranges, bci_ranges_constraint, num_procs=20, max_send=666628)
# find potential address of ByteCode Interpretter
merge = []
merge_method_syms_only = []
merge_klass_syms_only = []
merge_oop_thread_jthread_only = []
merge_oop_thread_jthread_method = []
for addr_val_lst in cpc_addr_uses.values():
    for addr, val in addr_val_lst:
        merge.append((addr, 'C', val))

for addr_val_lst in thread_addr_uses.values():
    for addr, val in addr_val_lst:
        merge.append((addr, 'T', val))
        merge_oop_thread_jthread_only.append((addr, 'T', val))
        merge_oop_thread_jthread_method.append((addr, 'T', val))

for addr_val_lst in oop_thread_addr_uses.values():
    for addr, val in addr_val_lst:
        merge.append((addr, 'O', val))
        merge_oop_thread_jthread_only.append((addr, 'O', val))
        merge_oop_thread_jthread_method.append((addr, 'O', val))

for addr_val_lst in method_addr_uses.values():
    for addr, val in addr_val_lst:
        merge.append((addr, 'M', val))
        merge_method_syms_only.append((addr, 'M', val))
        merge_oop_thread_jthread_method.append((addr, 'M', val))

for addr_val_lst in klass_addr_uses.values():
    for addr, val in addr_val_lst:
        merge.append((addr, 'K', val))
        merge_klass_syms_only.append((addr, 'K', val))
for addr_val_lst in sym_addr_uses.values():
    for addr, val in addr_val_lst:
        merge.append((addr, 'S', val))
        merge_method_syms_only.append((addr, 'S', val))
        merge_klass_syms_only.append((addr, 'S', val))


merge = sorted(merge, key=lambda item: item[0])
merge_method_syms_only = sorted(merge_method_syms_only, key=lambda item: item[0])
merge_klass_syms_only = sorted(merge_klass_syms_only, key=lambda item: item[0])
merge_oop_thread_jthread_only = sorted(merge_oop_thread_jthread_only, key=lambda item: item[0])
merge_oop_thread_jthread_method = sorted(merge_oop_thread_jthread_method, key=lambda item: item[0])

s = ''.join([v[1] for v in merge])
s_method_syms = ''.join([v[1] for v in merge_method_syms_only])
s_klass_syms = ''.join([v[1] for v in merge_klass_syms_only])
s_thread_oop = ''.join([v[1] for v in merge_oop_thread_jthread_only])
s_thread_oop_method = ''.join([v[1] for v in merge_oop_thread_jthread_method])

pos = s_method_syms.find('SM')
while pos != -1:
    e0 = merge_method_syms_only[pos]
    e1 = merge_method_syms_only[pos+1]
    sym = recoop_1.jva.symboltable_values[e0[2]]
    m =  recoop_1.jva.known_metas[e1[2]]
    m_str = str(m.klass_holder_value)+'.'+str(m.name())
    print ("Position: SM=%5d (0x%08x),  S-M = %8d: Symbol = %s Method = %s"%(pos, e0[0], (e0[0] - e1[0])/4, sym, m_str))
    pos = s_method_syms.find('SM', pos+1)

pos = s_method_syms.find('MS')
while pos != -1:
    e1 = merge_method_syms_only[pos]
    e0 = merge_method_syms_only[pos+1]
    sym = recoop_1.jva.symboltable_values[e0[2]]
    m =  recoop_1.jva.get_method_only(e1[2])
    m_str = str(m.klass_holder_value)+'.'+str(m.name())
    print ("Position: SM=%5d (0x%08x),  M-S = %8d: Symbol = %s Method = %s"%(pos, e1[0], (e1[0] - e0[0])/4, sym, m_str))
    pos = s_method_syms.find('MS', pos+1)

pos = s_klass_syms.find('SK')
while pos != -1:
    e0 = merge_klass_syms_only[pos]
    e1 = merge_klass_syms_only[pos+1]
    sym = recoop_1.jva.symboltable_values[e0[2]]
    klass =  recoop_1.jva.loaded_classes_by_addr[e1[2]]
    print ("Position: SK=%5d (0x%08x),  S-K = %8d: Symbol = %s Klass = %s"%(pos, e0[0], (e0[0] - e1[0])/4, sym, klass))
    pos = s_klass_syms.find('SK', pos+1)


pos = s_klass_syms.find('KS')
while pos != -1:
    e1 = merge_klass_syms_only[pos]
    e0 = merge_klass_syms_only[pos+1]
    sym = recoop_1.jva.symboltable_values[e0[2]]
    klass =  recoop_1.jva.loaded_classes_by_addr[e1[2]]
    print ("Position: KS=%5d (0x%08x),  K-S = %8d: Symbol = %s Klass = %s"%(pos, e1[0], (e1[0] - e0[0])/4, sym, klass))
    pos = s_klass_syms.find('KS', pos+1)


pos = s_thread_oop.find('TO')
while pos != -1:
    i_jthread = merge_oop_thread_jthread_only[pos]
    i_othread = merge_oop_thread_jthread_only[pos+1]
    addr = i_jthread[0]
    r = recoop_1.jva.find_range(addr)
    block_sz = 40
    word_sz = 4
    offset = 0
    addr = i_jthread[0]
    diff = i_othread[0]-i_jthread[0]
    if abs(diff)/4 > 4:
        header = "Position: TO=%5d (0x%08x),  T-O = %8d:"%(pos, addr, (diff)/4)
        print (header)
        pos = s_thread_oop.find('TO', pos+1)
        continue

    dwords = r.read_dwords_at_addr(addr, block_sz)
    block_data = {'addr':addr, 'dwords':dwords, 'word_sz':word_sz, 'offset':offset}
    
    pos_ = ["%4d"%i for i in xrange(0, block_sz)]
    addrs = ["0x%08x"%i for i in xrange(baddr, baddr+block_sz*4, word_sz)]
    addrs[0] = addrs[0] + "  <==="
    addrs = addrs[:1] + [i+ "      " for i in addrs[1:]]
    dwords = ["0x%08x"%i for i in block_data['dwords']]
    extras = [get_extra(recoop_inf, i) for i in block_data['dwords']]    
    dumps = zip(pos_, addrs, dwords, extras)
    
    dump_str = "\n".join(["%s %s %s %10s  %s"%(i[0], i[1], i[2], '', i[3]) for i in dumps])
    header = "Position: TO=%5d (0x%08x),  T-O = %8d:"%(pos, addr, (diff)/4)
    print ("%s\n%s\n"%(header, dump_str))
    pos = s_thread_oop.find('TO', pos+1)

blocks = {}
the_used_addr_r = [v for v in sym_addr_uses.values() if len(v) > 0]
for a_v_list in the_used_addr_r:
    addr_val_list = addr_val_list + a_v_list

addr_val_list = []
for a_v_list in the_used_addr_r:
    addr_val_list = addr_val_list + a_v_list

for addr, val in addr_val_list:
    sym = recoop_1.jva.symboltable_values[val]
    r = recoop_1.jva.find_range(addr)
    dwords = r.read_dwords_at_addr(addr-4*5,7)
    blocks[addr] = {'dwords':dwords, 'name':str(sym), 'val':val, 'offset':-4*5, 'word_sz':4}

res_dict = analyse_block_structure(blocks)

addrs = blocks.keys()
kv_blocks = [(k, blocks[k]) for k in addrs]

for addr,block_data in  kv_blocks[-1000:-800]:
    sym = recoop_1.jva.symboltable_values[block_data['dwords'][-2]]
    block_sz = len(block_data['dwords'])
    offset = block_data['offset']
    word_sz = block_data['word_sz']
    baddr = addr-offset
    pos_ = ["%4d"%i for i in xrange(0, block_sz)]
    addrs = ["0x%08x"%i for i in xrange(baddr, baddr+block_sz*4, word_sz)]
    dwords = ["0x%08x"%i for i in block_data['dwords']]
    extras = [get_extra(recoop_inf, i) for i in block_data['dwords']]    
    dumps = zip(pos_, addrs, dwords, extras)
    dump_str = "\n".join(["%s %s %s %10s  %s"%(i[0], i[1], i[2], '', i[3]) for i in dumps])
    print ("%s\n%s\n"%(sym, dump_str))

%cpaste
def read_blocks_twenty_one_twenty(recoop_inf, thread_addrs, thread_addr_uses):
    ptrs = [i for i in thread_addrs if i > 0]
    blocks_forty_1 = dict([(i, {}) for i in ptrs])
    for baddr, addr_jthread_tup in thread_addr_uses.items():
        r = recoop_inf.jva.find_range(baddr)
        for addr, jthread in addr_jthread_tup:
            if not jthread in ptrs:
                continue
            block = r.read_dwords_at_addr(addr, 40*4)
            blocks_forty_1[jthread][addr] = block
    return blocks_forty_1
--

%cpaste
def analyse_block_structure(recoop_inf, blocks, word_sz=4):
    valid_addrs = set()
    addr_occurence = {}
    addr_uses = {}
    val_occurrence = {}
    val_uses = {}
    # accumulate addrs
    baddrs = []
    for ptr, baddr_blocks in blocks.items():
        #baddrs = addr_blocks.keys()
        for baddr, _blocks in baddr_blocks.items():
            valid_addrs |= set([baddr+i for i in xrange(0, len(_blocks), word_sz)])

    for jthread, addr_blocks in blocks.items():
        for baddr, _blocks in addr_blocks.items():
            pos = 0
            for val in _blocks:
                addr = pos + baddr
                if val in valid_addrs:
                    if not val in addr_occurence:
                        addr_occurence[val] = {'tot':0}
                        addr_uses[val] = []
                    if not pos in addr_occurence[val]:
                    	addr_occurence[val][pos] = 0
                    addr_occurence[val]['tot'] += 1
                    addr_occurence[val][pos] += 1
                    addr_uses[val].append((baddr, addr))
                if not val in val_occurrence:
                    val_occurrence[val] = {'tot':0}
                    val_uses[val] = []
                if not pos in val_occurrence[val]:
                    val_occurrence[val][pos] = 0
                val_occurrence[val]['tot'] += 1
                val_occurrence[val][pos] += 1
                val_uses[val].append((baddr, addr))
                pos += 1
    res_dict = {}
    res_dict['addr_uses'] = addr_uses
    res_dict['addr_occurrence'] = addr_occurence
    res_dict['val_uses'] = val_uses
    res_dict['val_occurrence'] = val_occurrence
    return res_dict
--


def find_known_objects(recoop_inf, rem_block, baddr=0, word_sz=4):
    jva = recoop_inf.jva
    res = []
    for v in rem_block:
        #m = jva.get_method_only(v)
        m = jva.get_meta_only(v)
        o = jva.lookup_known_oop_only(v)
        k = jva.lookup_known_klass(v)
        r = None
        if m:
            r = (baddr+pos*4, m)
        elif k:
            r = (baddr+pos*4, k)
        elif o:
        	r = (baddr+pos*4, o)
        res.append(r)
        pos += 1
    return res

def contains_method_cpc(recoop_inf, rem_block):
    res = False
    jva = recoop_inf.jva
    p1 = sum([1 for i in rem_block if jva.get_method_only(i)])
    p2 = sum([1 for i in rem_block if jva.get_cpcache_only(i)])
    return p1 > 0 and p2 > 0

def contains_methods(recoop_inf, rem_block):
    res = False
    jva = recoop_inf.jva
    p1 = sum([1 for i in rem_block if jva.get_method_only(i)])
    return p1 > 0

def contains_cpc(recoop_inf, rem_block):
    res = False
    jva = recoop_inf.jva
    p1 = sum([1 for i in rem_block if jva.get_cpcache_only(i)])
    return p1 > 0



def filter_blocks(recoop_inf, blocks, filter_fn):
    res = {}
    for jthread, addr_blocks in blocks.items():
        for baddr, _blocks in addr_blocks.items():
            if filter_fn(recoop_inf, _blocks):
               res[baddr] = _blocks
    return res



def get_n_highest_pos(occurrences, num=4):
    occ_list = [(k, v) for k,v in occurrences.items() if k != 'tot' and v > 0 ]
    occ_list = sorted(occ_list , key=lambda t:t[1], reverse=True)
    return occ_list[:num]

def get_n_highest_pos_str(occurrences, num=4):
    return ", ".join(["[pos:%4d=%3d]"%(i[0], i[1]) for i in get_n_highest_pos(occurrences, num)])


blocks = read_blocks_twenty_one_twenty(recoop_1, thread_addrs, thread_addr_uses)
res_dict = analyse_block_structure(recoop_1, blocks, word_sz=4)




addr_occ = res_dict['addr_occurrence']
more_than_1 = sorted([(k, v['tot']) for k,v in addr_occ.items() if v['tot'] > 1], key=lambda t:t[1], reverse=True)
more_one_pos = [(k, v, get_n_highest_pos(addr_occ[k])) for k,v in more_than_1]
more_than_1_pos = sorted([(k, v, pos) for k,v,pos in more_one_pos if pos[0][1] > 4], key=lambda t:t[1], reverse=True)

print ("\n".join( [ "0x%08x: %5d: %s"%(i[0], i[1], get_n_highest_pos_str(addr_occ[i[0]])) for i in more_than_1_pos if i[1] > 8]))


%cpaste
comparison = []
for _block in values:
    info = []
    for i in _block:
        v = 0
        v = 'T' if i in thread_addrs else v
        v = 'M' if recoop_1.jva.get_method_only(i) != None else v
        v = 'C' if recoop_1.jva.get_cpcache_only(i) != None else v
        v = 'O' if v == 0 and recoop_1.jva.get_meta_only(i) else v
        info.append(v)
    comparison.append(info)
--

%cpaste
comparison = []
for _block in values:
    info = []
    for i in _block:
        v = 0
        v = 'T' if i in thread_addrs else v
        v = 'M' if recoop_1.jva.get_method_only(i) != None else v
        v = 'C' if recoop_1.jva.get_cpcache_only(i) != None else v
        v = 'O' if v == 0 and recoop_1.jva.get_meta_only(i) else v
        info.append(v)
    comparison.append(info)
--


def get_extra(recoop_inf, addr):
    if addr > 0 and addr in recoop_inf.jthread_start_mapping_thread_oop:
        oop_addr = recoop_inf.jthread_start_mapping_thread_oop[addr]
        thread_name = recoop_1.thread_infos[oop_addr]['name']
        return "Thread Native: %s @ Heap: 0x%08x"%(thread_name, oop_addr)
    elif addr > 0 and addr in recoop_inf.thread_oop_mapping_jthread:
        oop_addr = addr
        #oop_addr = recoop_inf.jthread_start_mapping_thread_oop[addr]
        thread_name = recoop_1.thread_infos[oop_addr]['name']
        return "JavaThread OOP: %s @ Heap: 0x%08x"%(thread_name, oop_addr)
    elif addr in recoop_inf.jva.known_oops:
        return "Oop: %s"%recoop_inf.jva.known_oops[addr]
    elif addr in recoop_inf.jva.loaded_classes_by_addr:
        return "Klass: %s"%recoop_inf.jva.loaded_classes_by_addr[addr]
    elif addr in recoop_inf.jva.symboltable_values:
        return "Symbol: %s"%recoop_inf.jva.symboltable_values[addr]
    elif addr in recoop_inf.jva.known_metas and \
         recoop_inf.jva.known_metas[addr]._name == 'Method':
        m = recoop_inf.jva.known_metas[addr]
        return "Method: %s.%s"%(m.klass_holder_value, m.name())
    elif addr in recoop_inf.jva.known_metas and \
         recoop_inf.jva.known_metas[addr]._name == 'ConstantPoolCache':
        cpc = recoop_inf.jva.known_metas[addr]
        cp = recoop_inf.jva.known_metas[cpc.contant_pool]
        return "CPCache: %s"%(cp.pool_holder_value)
    elif addr in recoop_inf.jva.known_metas and \
         recoop_inf.jva.known_metas[addr]._name == 'ConstantPool':
        cp = recoop_inf.jva.known_metas[addr]
        return "CP: %s"%(cp.pool_holder_value)
    elif addr in recoop_inf.jva.known_metas:
        meta = recoop_inf.jva.known_metas[addr]
        return "%s"%(meta._name)
    return ''


for addr,block_data in  kv_blocks[0:100]:
    sym = recoop_1.jva.symboltable_values[block_data['dwords'][-1]]
    block_sz = len(block_data['dwords'])
    offset = block_data['offset']
    word_sz = block_data['word_sz']
    baddr = addr-offset
    pos_ = ["%4d"%i for i in xrange(0, block_sz)]
    addrs = ["0x%08x"%i for i in xrange(baddr, baddr+block_sz*4, word_sz)]
    dwords = ["0x%08x"%i for i in block_data['dwords']]
    extras = [get_extra(recoop_inf, i) for i in block_data['dwords']]    
    dumps = zip(pos_, addrs, dwords, extras)
    dump_str = "\n".join(["%s %s %s %10s  %s"%(i[0], i[1], i[2], '', i[3]) for i in dumps])
    print ("%s\n%s\n"%(sym, dump_str))


