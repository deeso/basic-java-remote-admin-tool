import socket, struct, base64, binascii, threading, time, select
from socket import  error
#from basic_java_remote_console import *

sock_listener_base = 48000

class SocketListenerServer(threading.Thread):
    def __init__(self, socket_listener, accept_one_client=True):
        threading.Thread.__init__(self)
        self.socket_listener = socket_listener
        self.keep_running = False
        self.one_client = accept_one_client

    def run(self):
        print ("Starting the server listener")
        self.keep_running = True
        while self.keep_running:
            c, a = self.socket_listener.sock.accept()
            sch = SocketClientHandler(c, self.socket_listener, 
                num_sends = self.socket_listener.num_sends, 
                size_sends = self.socket_listener.size_sends, 
                send_spacing = self.socket_listener.send_spacing)
            self.socket_listener.clients.append(sch)
            sch.start()
            if self.one_client:
                break


class SocketClientHandler(threading.Thread):
    def __init__(self, client_sock, socket_listener, num_sends = 100, size_sends=1024, send_spacing=1):
        threading.Thread.__init__(self)
        self.sock = client_sock
        self.sock.setblocking(0)
        self.listener = socket_listener
        self.num_sends = num_sends
        self.size_sends = size_sends
        self.send_spacing = send_spacing

    def run(self):
        host, port = self.sock.getpeername()
        print ("Starting Send/Recieve to: %s:%d"%(host,port))

        while self.num_sends > 0:
            data = self.listener.get_data(self.size_sends)
            print ("Sending 0x%x bytes to %s:%d"%(len(data), host, port))
            self.sock.send(data)
            if self.send_spacing > 0:
                print ("Sleeping for %ds"%(self.send_spacing))
                time.sleep(self.send_spacing)
            print ("Checking for data")
            rdata = self.perform_recv()
            print ("Recv'ing 0x%x bytes from %s:%d"%(len(rdata), host, port))
            self.num_sends += -1
        print ("Completed Send/Recieve to: %s:%d"%(host,port))

    def perform_recv(self):
        inputs = [self.sock]
        outputs = []
        result = None
        readable, writable, exceptional = select.select(inputs, outputs, inputs)
        if len(readable) > 0:
            print ("Data is available")
            data = self.sock.recv(1024)
            result = data
            print ("Data: "+result)
            while len(data) > 0:
                try:
                    data = self.sock.recv(1024)
                    result = result + data
                except socket.error as err:
                    print("HAD ERROR")
                    break

        return result

class SocketListener(object):
    def __init__(self, sock_id=None, data_size=8192, host='127.0.0.1', data=None,
                       num_sends = 100, size_sends=1024, send_spacing=1):
        global sock_listener_base
        self.sock_id = sock_id
        self.host = host
        if sock_id is None:
            self.sock_id = sock_listener_base
            sock_listener_base += 1
        self.data = data
        self.num_sends = num_sends
        self.size_sends = size_sends
        self.send_spacing = send_spacing
        self.clients = []


    def get_data(self, sz=0):
        if self.data is None:
            self.data = "s3cr3t_d4t3_%d"%self.sock_id
        if self.size_sends == 0:
            return (self.data*1024)[:1024]
        return (self.data*self.size_sends)[:1024]

    def start(self):
        self.sock = socket.socket()
        self.sock.bind((self.host, self.sock_id))
        self.sock.listen(10)
        self.cur_listener = SocketListenerServer(self)
        self.cur_listener.start()

#rat_host = "java-workx32-00"
#rat_ip = socket.gethostbyname('java-workx32-00.internal.thecoverofnight.com')
#num_servers = 1000



# start 10 sockets locally
num_listeners = 100
listeners = [SocketListener(sock_id=sock_listener_base+i, host="", num_sends=3, size_sends=4096) for i in xrange(0, 10)]
for l in listeners:
    l.start()






s = socket.socket()
s.connect(('', 59788))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=old_test_class))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=old_test2_class))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=old_test_class))
LoadClassStringCmd.read_message_deserialize(s)
LoadClassStringCmd.read_message_deserialize(s)
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=old_test_class))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=old_test_class))
LoadClassStringCmd.read_message_deserialize(s)
s = socket.socket()
s.connect(('', 59788))
LoadClassStringCmd.read_message_deserialize(s)
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=old_test2_class))
LoadClassStringCmd.read_message_deserialize(s)
s.send(CallCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
LoadClassStringCmd.read_message_deserialize(s)
s = socket.socket()
s.connect(('', 59788))
s = socket.socket()
s.connect(('', 59788))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=old_test2_class))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=old_test_class))
LoadClassStringCmd.read_message_deserialize(s)
LoadClassStringCmd.read_message_deserialize(s)
s.send(CallCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
CallCmd.read_message_deserialize(s)
data = open("/research_data/code/git/basic-java-remote-admin-tool/bin/Test2.class", 'r').read()
binascii.hexlify(data)
data = open("/research_data/code/git/basic-java-remote-admin-tool/bin/Test.class", 'r').read()
binascii.hexlify(data)
data = open("/research_data/code/git/basic-java-remote-admin-tool/bin/Test.class", 'r').read()
binascii.hexlify(data)
binascii.hexlify(data)
data = open("/research_data/code/git/basic-java-remote-admin-tool/bin/Test2.class", 'r').read()
binascii.hexlify(data)
old_test_class = "cafebabe000000310036070002010004546573740700040100106a6176612f6c616e672f4f626a656374010004746573740100064c546573743b0100083c636c696e69743e010003282956010004436f64650a0001000b0c000c00080100063c696e69743e090001000e0c0005000601000f4c696e654e756d6265725461626c650100124c6f63616c5661726961626c655461626c650a0003000b09001300150700140100106a6176612f6c616e672f53797374656d0c001600170100036f75740100154c6a6176612f696f2f5072696e7453747265616d3b08001901001854657374206c6f61646564207375636365737366756c6c790a001b001d07001c0100136a6176612f696f2f5072696e7453747265616d0c001e001f0100077072696e746c6e010015284c6a6176612f6c616e672f537472696e673b295601000474686973010004646f6974010033284c4c6f616465723b4c436c69656e74436f6d6d616e6448616e646c65723b4c6a6176612f6c616e672f537472696e673b2956080024010010546573742e646f69742063616c6c65640100066c6f616465720100084c4c6f616465723b010014636c69656e74436f6d6d616e6448616e646c65720100164c436c69656e74436f6d6d616e6448616e646c65723b010006706172616d730100124c6a6176612f6c616e672f537472696e673b010016285b4c6a6176612f6c616e672f4f626a6563743b29560100135b4c6a6176612f6c616e672f4f626a6563743b0100046d61696e010016285b4c6a6176612f6c616e672f537472696e673b29560a000100300c0021002b010004617267730100135b4c6a6176612f6c616e672f537472696e673b0100036f626a01000a536f7572636546696c65010009546573742e6a6176610021000100030000000100090005000600000005000800070008000100090000002b000200000000000bbb000159b7000ab3000db100000002000f0000000600010000000300100000000200000001000c0008000100090000003f000200010000000d2ab70011b200121218b6001ab100000002000f0000000e00030000000500040006000c000700100000000c00010000000d002000060000000900210022000100090000004b0002000300000009b200121223b6001ab100000002000f0000000a00020000000a0008000b00100000002000030000000900250026000000000009002700280001000000090029002a000200090021002b00010009000000370002000100000009b200121223b6001ab100000002000f0000000a00020000000e0008000f00100000000c0001000000090029002c00000009002d002e0001000900000046000100020000000a06bd00034c2bb8002fb100000002000f0000000e000300000012000500140009001500100000001600020000000a003100320000000500050033002c000100010034000000020035";
old_test2_class = "cafebabe00000031003d07000201000554657374320700040100106a6176612f6c616e672f4f626a656374010004746573740100064c546573743b01000574657374320100083c636c696e69743e010003282956010004436f646507000c010004546573740a000b000e0c000f00090100063c696e69743e09000100110c0005000609000100130c0007000601000f4c696e654e756d6265725461626c650100124c6f63616c5661726961626c655461626c650a0003000e090018001a0700190100106a6176612f6c616e672f53797374656d0c001b001c0100036f75740100154c6a6176612f696f2f5072696e7453747265616d3b08001e0100195465737432206c6f61646564207375636365737366756c6c790a002000220700210100136a6176612f696f2f5072696e7453747265616d0c002300240100077072696e746c6e010015284c6a6176612f6c616e672f537472696e673b2956010004746869730100074c54657374323b010004646f6974010016285b4c6a6176612f6c616e672f4f626a6563743b29560a000b002a0c0027002808002c01001154657374322e646f69742063616c6c6564010006706172616d730100135b4c6a6176612f6c616e672f4f626a6563743b010033284c4c6f616465723b4c436c69656e74436f6d6d616e6448616e646c65723b4c6a6176612f6c616e672f537472696e673b29560a000b00310c0027002f0100066c6f616465720100084c4c6f616465723b010014636c69656e74436f6d6d616e6448616e646c65720100164c436c69656e74436f6d6d616e6448616e646c65723b0100124c6a6176612f6c616e672f537472696e673b0100046d61696e010016285b4c6a6176612f6c616e672f537472696e673b2956010004617267730100135b4c6a6176612f6c616e672f537472696e673b01000a536f7572636546696c6501000a54657374322e6a617661002100010003000000020009000500060000000900070006000000050008000800090001000a000000390002000000000015bb000b59b7000db30010bb000b59b7000db30012b10000000200140000000a000200000003000a000400150000000200000001000f00090001000a0000003f000200010000000d2ab70016b20017121db6001fb10000000200140000000e00030000000500040006000c000700150000000c00010000000d0025002600000009002700280001000a0000003f000200010000000d2ab80029b20017122bb6001fb10000000200140000000e00030000000b0004000c000c000d00150000000c00010000000d002d002e000000090027002f0001000a00000055000300030000000f2a2b2cb80030b20017122bb6001fb10000000200140000000e00030000000f00060010000e001100150000002000030000000f0032003300000000000f0034003500010000000f002d003600020009003700380001000a000000350003000100000007010101b80030b10000000200140000000a0002000000140006001500150000000c0001000000070039003a00000001003b00000002003c";
test_class = "cafebabe000000310038070002010004546573740700040100106a6176612f6c616e672f4f626a656374010004746573740100064c546573743b0100083c636c696e69743e010003282956010004436f64650a0001000b0c000c00080100063c696e69743e090001000e0c0005000601000f4c696e654e756d6265725461626c650100124c6f63616c5661726961626c655461626c650a0003000b09001300150700140100106a6176612f6c616e672f53797374656d0c001600170100036f75740100154c6a6176612f696f2f5072696e7453747265616d3b08001901001b54657374207633206c6f61646564207375636365737366756c6c790a001b001d07001c0100136a6176612f696f2f5072696e7453747265616d0c001e001f0100077072696e746c6e010015284c6a6176612f6c616e672f537472696e673b29560100047468697301000b676574496e7374616e636501000828294c546573743b010004646f6974010033284c4c6f616465723b4c436c69656e74436f6d6d616e6448616e646c65723b4c6a6176612f6c616e672f537472696e673b2956080026010010546573742e646f69742063616c6c65640100066c6f616465720100084c4c6f616465723b010014636c69656e74436f6d6d616e6448616e646c65720100164c436c69656e74436f6d6d616e6448616e646c65723b010006706172616d730100124c6a6176612f6c616e672f537472696e673b010016285b4c6a6176612f6c616e672f4f626a6563743b29560100135b4c6a6176612f6c616e672f4f626a6563743b0100046d61696e010016285b4c6a6176612f6c616e672f537472696e673b29560a000100320c0023002d010004617267730100135b4c6a6176612f6c616e672f537472696e673b0100036f626a01000a536f7572636546696c65010009546573742e6a6176610021000100030000000100190005000600000006000800070008000100090000002b000200000000000bbb000159b7000ab3000db100000002000f0000000600010000000300100000000200000002000c0008000100090000003f000200010000000d2ab70011b200121218b6001ab100000002000f0000000e00030000000500040006000c000700100000000c00010000000d00200006000000080021002200010009000000240001000000000004b2000db000000002000f0000000600010000000a0010000000020000000900230024000100090000004b0002000300000009b200121225b6001ab100000002000f0000000a00020000000d0008000e001000000020000300000009002700280000000000090029002a000100000009002b002c000200090023002d00010009000000370002000100000009b200121225b6001ab100000002000f0000000a0002000000110008001200100000000c000100000009002b002e00000009002f00300001000900000046000100020000000a06bd00034c2bb80031b100000002000f0000000e000300000015000500170009001800100000001600020000000a003300340000000500050035002e000100010036000000020037";
test2_class = "cafebabe00000031004207000201000554657374320700040100106a6176612f6c616e672f4f626a656374010004746573740100064c546573743b01000574657374320100074c54657374323b0100083c636c696e69743e010003282956010004436f64650a000d000f07000e010004546573740c0010001101000b676574496e7374616e636501000828294c546573743b09000100130c000500060a000100150c0016000a0100063c696e69743e09000100180c0007000801000f4c696e654e756d6265725461626c650100124c6f63616c5661726961626c655461626c650a0003001509001d001f07001e0100106a6176612f6c616e672f53797374656d0c002000210100036f75740100154c6a6176612f696f2f5072696e7453747265616d3b08002301001c5465737432207633206c6f61646564207375636365737366756c6c790a002500270700260100136a6176612f696f2f5072696e7453747265616d0c002800290100077072696e746c6e010015284c6a6176612f6c616e672f537472696e673b29560100047468697301000928294c54657374323b010004646f6974010016285b4c6a6176612f6c616e672f4f626a6563743b29560a000d002f0c002c002d08003101001454657374322e646f69742076332063616c6c6564010006706172616d730100135b4c6a6176612f6c616e672f4f626a6563743b010033284c4c6f616465723b4c436c69656e74436f6d6d616e6448616e646c65723b4c6a6176612f6c616e672f537472696e673b29560a000d00360c002c00340100066c6f616465720100084c4c6f616465723b010014636c69656e74436f6d6d616e6448616e646c65720100164c436c69656e74436f6d6d616e6448616e646c65723b0100124c6a6176612f6c616e672f537472696e673b0100046d61696e010016285b4c6a6176612f6c616e672f537472696e673b2956010004617267730100135b4c6a6176612f6c616e672f537472696e673b01000a536f7572636546696c6501000a54657374322e6a6176610021000100030000000200090005000600000019000700080000000600080009000a0001000b000000350002000000000011b8000cb30012bb000159b70014b30017b10000000200190000000a00020000000300060004001a00000002000000020016000a0001000b0000003f000200010000000d2ab7001bb2001c1222b60024b10000000200190000000e00030000000500040006000c0007001a0000000c00010000000d002a00080000000a0010002b0001000b000000240001000000000004b20017b00000000200190000000600010000000a001a0000000200000009002c002d0001000b0000003f000200010000000d2ab8002eb2001c1230b60024b10000000200190000000e00030000000f00040010000c0011001a0000000c00010000000d0032003300000009002c00340001000b00000055000300030000000f2a2b2cb80035b2001c1230b60024b10000000200190000000e00030000001300060014000e0015001a0000002000030000000f0037003800000000000f0039003a00010000000f0032003b00020009003c003d0001000b000000350003000100000007010101b80035b10000000200190000000a00020000001800060019001a0000000c000100000007003e003f000000010040000000020041";
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=old_test_class))
LoadClassStringCmd.read_message_deserialize(s)
s = socket.socket()
s.connect(('', 59788))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=old_test_class))
LoadClassStringCmd.read_message_deserialize(s)
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=old_test2_class))
LoadClassStringCmd.read_message_deserialize(s)
s.send(CallCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=test_class))
s = socket.socket()
s.connect(('', 59788))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=old_test_class))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=test_class))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=test2_class))
s.send(CallCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s = socket.socket()
s.connect(('', 59788))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=old_test2_class))
s.recv(8192)
s.recv(8192)
s.send(CallCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
%cpaste
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=test_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s = socket.socket()
s.connect(('', 59788))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s.recv(8192)
s = socket.socket()
s = socket(('', 59788))
s = socket.socket()
s.connect(('', 59788))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=test_class))
s.recv(8192)
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=test_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s = socket.socket()
s.connect(('', 59788))
s = socket.socket()
s.connect(('', 59788))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=old_test2_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=test2_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s = socket.socket()
s.connect(('', 59788))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=test_class))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=old_test2_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s = socket.socket()
s.connect(('', 59788))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=test_class))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=old_test2_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s.recv(8192)
s.recv(8192)
s = socket.socket()
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=test2_class))
s.connect(('', 59788))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=test2_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s = socket.socket()
s.connect(('', 59788))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=test2_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s = socket.socket()
s.connect(('', 59788))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=test_class))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=test2_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
data = open("/research_data/code/git/basic-java-remote-admin-tool/bin/Test2.class", 'r').read()
binascii.hexlify(data)
old_test2_class = "cafebabe00000031004207000201000554657374320700040100106a6176612f6c616e672f4f626a656374010004746573740100064c546573743b01000574657374320100074c54657374323b0100083c636c696e69743e010003282956010004436f64650a000d000f07000e010004546573740c0010001101000b676574496e7374616e636501000828294c546573743b09000100130c000500060a000100150c0016000a0100063c696e69743e09000100180c0007000801000f4c696e654e756d6265725461626c650100124c6f63616c5661726961626c655461626c650a0003001509001d001f07001e0100106a6176612f6c616e672f53797374656d0c002000210100036f75740100154c6a6176612f696f2f5072696e7453747265616d3b08002301001c5465737432207633206c6f61646564207375636365737366756c6c790a002500270700260100136a6176612f696f2f5072696e7453747265616d0c002800290100077072696e746c6e010015284c6a6176612f6c616e672f537472696e673b29560100047468697301000928294c54657374323b010004646f6974010016285b4c6a6176612f6c616e672f4f626a6563743b29560a000d002f0c002c002d08003101001454657374322e646f69742076322063616c6c6564010006706172616d730100135b4c6a6176612f6c616e672f4f626a6563743b010033284c4c6f616465723b4c436c69656e74436f6d6d616e6448616e646c65723b4c6a6176612f6c616e672f537472696e673b29560a000d00360c002c00340100066c6f616465720100084c4c6f616465723b010014636c69656e74436f6d6d616e6448616e646c65720100164c436c69656e74436f6d6d616e6448616e646c65723b0100124c6a6176612f6c616e672f537472696e673b0100046d61696e010016285b4c6a6176612f6c616e672f537472696e673b2956010004617267730100135b4c6a6176612f6c616e672f537472696e673b01000a536f7572636546696c6501000a54657374322e6a6176610021000100030000000200090005000600000019000700080000000600080009000a0001000b000000350002000000000011b8000cb30012bb000159b70014b30017b10000000200190000000a00020000000300060004001a00000002000000020016000a0001000b0000003f000200010000000d2ab7001bb2001c1222b60024b10000000200190000000e00030000000500040006000c0007001a0000000c00010000000d002a00080000000a0010002b0001000b000000240001000000000004b20017b00000000200190000000600010000000a001a0000000200000009002c002d0001000b0000003f000200010000000d2ab8002eb2001c1230b60024b10000000200190000000e00030000000f00040010000c0011001a0000000c00010000000d0032003300000009002c00340001000b00000055000300030000000f2a2b2cb80035b2001c1230b60024b10000000200190000000e00030000001300060014000e0015001a0000002000030000000f0037003800000000000f0039003a00010000000f0032003b00020009003c003d0001000b000000350003000100000007010101b80035b10000000200190000000a00020000001800060019001a0000000c000100000007003e003f000000010040000000020041";
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=old_test2_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
data = open("/research_data/code/git/basic-java-remote-admin-tool/bin/Test2.class", 'r').read()
old_test2_class = binascii.hexlify(data)
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=old_test2_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=old_test2_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=old_test2_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=old_test2_class))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=old_test2_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s = socket.socket()
s.connect(('', 59788))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=old_test2_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=test_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s = socket.socket()
s.connect(('', 59788))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=test_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s = socket.socket()
s.connect(('', 59788))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=test_class))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=old_test2_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=test2_class))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=test2_class))
s = socket.socket()
s.connect(('', 59788))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=old_test2_class))
s = socket.socket()
s.connect(('', 59788))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=old_test2_class))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=test2_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s = socket.socket()
s.connect(('', 59788))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=old_test2_class))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=test2_class))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test", klass_data_hex=test_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
s.send(LoadClassStringCmd.serialize(10, klass_name="Test2", klass_data_hex=old_test2_class))
s.send(CallStaticCmd.serialize(10, klass_name="Test2", method_name="doit", *[]))
%cpaste
s = socket.socket()
s.connect(('', 59788))
s.send(ConnectCmd.serialize(10, klass_name='127.0.0.1', port=56700))
s.send(ConnectCmd.serialize(10, host='127.0.0.1', port=56700))
s = socket.socket()
s.connect(('', 59788))
s.send(ConnectCmd.serialize(10, host='127.0.0.1', port=56700))
s = socket.socket()
s.connect(('', 59788))
s.send(ConnectCmd.serialize(10, host='127.0.0.1', port=56700))
s = socket.socket()
s.connect(('', 59788))
s = socket.socket()
s.send(ConnectCmd.serialize(10, host='127.0.0.1', port=56700))
s.connect(('', 59788))
s.send(ConnectCmd.serialize(10, host='127.0.0.1', port=56700))
s = socket(('', 59788))
s = socket.socket()
s.send(ConnectCmd.serialize(10, host='127.0.0.1', port=56700))
s.connect(('', 59788))
s.send(ConnectCmd.serialize(10, host='127.0.0.1', port=56700))
s.send(SendCmd.serialize(10, host='127.0.0.1', port=56700, content="Testing"))
ConnectCmd.read_message_deserialize(s)
SendCmd.read_message_deserialize(s)
s = socket.socket()
s.connect(('', 59788))
s.send(ConnectCmd.serialize(10, host='127.0.0.1', port=56700))
s.send(SendCmd.serialize(10, host='127.0.0.1', port=56700, content="Testing"))
s = socket.socket()
s.send(ConnectCmd.serialize(10, host='127.0.0.1', port=56700))
s = socket.socket()
s.connect(('', 59788))
s.send(ConnectCmd.serialize(10, host='127.0.0.1', port=56700))
ConnectCmd.read_message_deserialize(s)
%cpaste
s.send(CloseCmd.serialize(10, host='127.0.0.1', port=56700))
CloseCmd.read_message_deserialize(s)
s.send(CloseCmd.serialize(10, host='127.0.0.1', port=56700))
s = socket.socket()
s.connect(('', 59788))
s.send(ConnectCmd.serialize(10, host='127.0.0.1', port=56700))
ConnectCmd.read_message_deserialize(s)
s.send(SendCmd.serialize(10, host='127.0.0.1', port=56700, content="Testing"))
s = socket.socket()
s.connect(('', 59788))
s.send(ConnectCmd.serialize(10, host='127.0.0.1', port=56700))
s.send(SendCmd.serialize(10, host='127.0.0.1', port=56700, content="Testing"))
s.send(CloseCmd.serialize(10, host='127.0.0.1', port=56700))
CloseCmd.read_message_deserialize(s)
CloseCmd.read_message_deserialize(s)
s.send(ConnectCmd.serialize(10, host='127.0.0.1', port=56700))
ConnectCmd.read_message_deserialize(s)
s = socket.socket()
s.connect(('', 59788))
s.send(ConnectCmd.serialize(10, host='127.0.0.1', port=56700))
ConnectCmd.read_message_deserialize(s)
s.send(SendCmd.serialize(10, host='127.0.0.1', port=56700, content="Testing"))
s.send(RecvCmd.serialize(10, host='127.0.0.1', port=56700, content_len=1024))
s.send(RecvCmd.serialize(10, '127.0.0.1', 56700, content_len=1024))
s.send(RecvCmd.serialize(10, '127.0.0.1', port=56700, content_len=1024))
%cpaste
s.send(RecvCmd.serialize(10, '127.0.0.1', 56700, content_len=1024))
%cpaste
s.send(RecvCmd.serialize(10, '127.0.0.1', 56700, content_len=1024))
RecvCmd.read_message_deserialize(s)
s.send(RecvCmd.serialize(10, '127.0.0.1', 56700, content_len=1024))
RecvCmd.read_message_deserialize(s)
s.send(RecvCmd.serialize(10, '127.0.0.1', 56700, content_len=1024))
s.send(RecvCmd.serialize(10, '127.0.0.1', 56700, content_len=1024))
s = socket.socket()
s.connect(('', 59788))
s.send(ConnectCmd.serialize(10, host='127.0.0.1', port=56700))
ConnectCmd.read_message_deserialize(s)
s.send(SendCmd.serialize(10, host='127.0.0.1', port=56700, content="Testing"))
s.send(RecvCmd.serialize(10, '127.0.0.1', 56700, content_len=1024))
s.send(RecvCmd.serialize(10, '127.0.0.1', 56700, content_len=1024))
s = socket.socket()
s.connect(('', 59788))
s.send(ConnectCmd.serialize(10, host='127.0.0.1', port=56700))
ConnectCmd.read_message_deserialize(s)
s.send(SendCmd.serialize(10, host='127.0.0.1', port=56700, content="Testing"))
s.send(SendCmd.serialize(10, host='127.0.0.1', port=56700, content="Testing"))
SendCmd.read_message_deserialize(s)
s.send(SendCmd.serialize(10, host='127.0.0.1', port=56700, content="Testing"))
SendCmd.read_message_deserialize(s)
s.send(RecvCmd.serialize(10, '127.0.0.1', 56700, content_len=1024))
RecvCmd.read_message_deserialize(s)
s = socket.socket()
s.send(ConnectCmd.serialize(10, host='127.0.0.1', port=56700))
s.connect(('', 59788))
s.send(ConnectCmd.serialize(10, host='127.0.0.1', port=56700))
ConnectCmd.read_message_deserialize(s)
s.send(SendCmd.serialize(10, host='127.0.0.1', port=56700, content="Testing"))
s.send(RecvCmd.serialize(10, '127.0.0.1', 56700, content_len=1024))
SendCmd.read_message_deserialize(s)
RecvCmd.read_message_deserialize(s)
s = socket.socket()
s.connect(('', 59788))
s.send(ConnectCmd.serialize(10, host='127.0.0.1', port=56700))
