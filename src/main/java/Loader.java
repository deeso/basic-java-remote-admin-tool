import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.jar.JarFile;






public class Loader {
	static Loader ldr = new Loader();
	final static int DEFAULT_PORT = 59788;
	private static final int DEFAULT_RUNTIME_SECONDS = 86400;

	
	private static Loader loaderInstance = new Loader();
	//public static String example = "cafebabe000000310036070002010004546573740700040100106a6176612f6c616e672f4f626a656374010004746573740100064c546573743b0100083c636c696e69743e010003282956010004436f64650a0001000b0c000c00080100063c696e69743e090001000e0c0005000601000f4c696e654e756d6265725461626c650100124c6f63616c5661726961626c655461626c650a0003000b09001300150700140100106a6176612f6c616e672f53797374656d0c001600170100036f75740100154c6a6176612f696f2f5072696e7453747265616d3b08001901001854657374206c6f61646564207375636365737366756c6c790a001b001d07001c0100136a6176612f696f2f5072696e7453747265616d0c001e001f0100077072696e746c6e010015284c6a6176612f6c616e672f537472696e673b295601000474686973010004646f6974010033284c4c6f616465723b4c436c69656e74436f6d6d616e6448616e646c65723b4c6a6176612f6c616e672f537472696e673b2956080024010010546573742e646f69742063616c6c65640100066c6f616465720100084c4c6f616465723b010014636c69656e74436f6d6d616e6448616e646c65720100164c436c69656e74436f6d6d616e6448616e646c65723b010006706172616d730100124c6a6176612f6c616e672f537472696e673b010016285b4c6a6176612f6c616e672f4f626a6563743b29560100135b4c6a6176612f6c616e672f4f626a6563743b0100046d61696e010016285b4c6a6176612f6c616e672f537472696e673b29560a000100300c0021002b010004617267730100135b4c6a6176612f6c616e672f537472696e673b0100036f626a01000a536f7572636546696c65010009546573742e6a6176610021000100030000000100090005000600000005000800070008000100090000002b000200000000000bbb000159b7000ab3000db100000002000f0000000600010000000300100000000200000001000c0008000100090000003f000200010000000d2ab70011b200121218b6001ab100000002000f0000000e00030000000500040006000c000700100000000c00010000000d002000060000000900210022000100090000004b0002000300000009b200121223b6001ab100000002000f0000000a00020000000a0008000b00100000002000030000000900250026000000000009002700280001000000090029002a000200090021002b00010009000000370002000100000009b200121223b6001ab100000002000f0000000a00020000000e0008000f00100000000c0001000000090029002c00000009002d002e0001000900000046000100020000000a06bd00034c2bb8002fb100000002000f0000000e000300000012000500140009001500100000001600020000000a003100320000000500050033002c000100010034000000020035";
	//public static String example2 = "cafebabe00000031003d07000201000554657374320700040100106a6176612f6c616e672f4f626a656374010004746573740100064c546573743b01000574657374320100083c636c696e69743e010003282956010004436f646507000c010004546573740a000b000e0c000f00090100063c696e69743e09000100110c0005000609000100130c0007000601000f4c696e654e756d6265725461626c650100124c6f63616c5661726961626c655461626c650a0003000e090018001a0700190100106a6176612f6c616e672f53797374656d0c001b001c0100036f75740100154c6a6176612f696f2f5072696e7453747265616d3b08001e0100195465737432206c6f61646564207375636365737366756c6c790a002000220700210100136a6176612f696f2f5072696e7453747265616d0c002300240100077072696e746c6e010015284c6a6176612f6c616e672f537472696e673b2956010004746869730100074c54657374323b010004646f6974010016285b4c6a6176612f6c616e672f4f626a6563743b29560a000b002a0c0027002808002c01001154657374322e646f69742063616c6c6564010006706172616d730100135b4c6a6176612f6c616e672f4f626a6563743b010033284c4c6f616465723b4c436c69656e74436f6d6d616e6448616e646c65723b4c6a6176612f6c616e672f537472696e673b29560a000b00310c0027002f0100066c6f616465720100084c4c6f616465723b010014636c69656e74436f6d6d616e6448616e646c65720100164c436c69656e74436f6d6d616e6448616e646c65723b0100124c6a6176612f6c616e672f537472696e673b0100046d61696e010016285b4c6a6176612f6c616e672f537472696e673b2956010004617267730100135b4c6a6176612f6c616e672f537472696e673b01000a536f7572636546696c6501000a54657374322e6a617661002100010003000000020009000500060000000900070006000000050008000800090001000a000000390002000000000015bb000b59b7000db30010bb000b59b7000db30012b10000000200140000000a000200000003000a000400150000000200000001000f00090001000a0000003f000200010000000d2ab70016b20017121db6001fb10000000200140000000e00030000000500040006000c000700150000000c00010000000d0025002600000009002700280001000a0000003f000200010000000d2ab80029b20017122bb6001fb10000000200140000000e00030000000b0004000c000c000d00150000000c00010000000d002d002e000000090027002f0001000a00000055000300030000000f2a2b2cb80030b20017122bb6001fb10000000200140000000e00030000000f00060010000e001100150000002000030000000f0032003300000000000f0034003500010000000f002d003600020009003700380001000a000000350003000100000007010101b80030b10000000200140000000a0002000000140006001500150000000c0001000000070039003a00000001003b00000002003c";

	public static ServerSocket myServerSocket = null;
	public static int myServerPort = DEFAULT_PORT;
	public Boolean myRunServerSocket = false;
	private boolean myServerSocketStarted;
	final ExecutorService clientProcessingPool = Executors
			.newFixedThreadPool(10);
	private HashMap<String, ClientCommandHandler> mySocketMap = new HashMap<String, ClientCommandHandler>();
	
	public void start() {
		Loader ldr = getLoaderInstance();
		if (!ldr.myRunServerSocket && !ldr.myServerSocketStarted) {
			ldr.myServerSocketStarted = true;
			ldr.serverTask.start();
			if (myConnectHost != null && myConnectHost.length() > 0) {
				ldr.clientTask.start();
			}
		}
	}
	
	public static Loader getLoaderInstance() {
		if (loaderInstance == null)
			loaderInstance = new Loader();
		return loaderInstance;
	}
	
	@SuppressWarnings("unchecked")
	public Class<CommandHandler> isCommandHandler(String className) {
		try {
			Class <?> klass = myBCL.loadClass(className);
			if (CommandHandler.class.isAssignableFrom(klass)) {
				return (Class<CommandHandler>)klass;
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}
	

	Thread clientTask = new Thread() {
		public void run() {
			boolean connected = false;
			if (myConnectHost.length() == 0) return;
			
			while (!connected) {
				try {					
					Socket clientSocket = new Socket(myConnectHost, myConnectHostPort);
					connected = true;
					ClientCommandHandler cmdHandler = new ClientCommandHandler(clientSocket);
					cmdHandler.setCommands();
					clientProcessingPool.execute(cmdHandler);
				} catch (IOException e) {
					System.err.println("Failed to connect to: "+myConnectHost+":"+myConnectHostPort);
					e.printStackTrace();
				} catch (IllegalAccessError e) {
					System.err.println("Accept failed.");
					e.printStackTrace();					
				}
				if (!connected) {
					try {
						Thread.sleep(6000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}

		}
	};
	
	Thread serverTask = new Thread() {
		public void run() {
			myRunServerSocket = true;
			try {
				myServerSocket = new ServerSocket(myServerPort);
			} catch (IOException e) {
				System.err.println("Accept failed.");
			}
			while (myRunServerSocket) {
				try {					
					Socket clientSocket = myServerSocket.accept();
					ClientCommandHandler cmdHandler = new ClientCommandHandler(clientSocket);
					cmdHandler.setCommands();
					clientProcessingPool.execute(cmdHandler);
				} catch (IOException e) {
					System.err.println("Accept failed.");
					e.printStackTrace();
				} catch (IllegalAccessError e) {
					System.err.println("Accept failed.");
					e.printStackTrace();					
				}
//				catch (InterruptedException ex) {
//					System.err.println("Accept failed, interrupted.");
//				}
			}

		}
	};

	public boolean startServerSocket(int port) {

		return false;
	}
	protected byte [] b64Decode(String data) {
		return Base64.getDecoder().decode(data);
	}
	
	protected String b64Encode(byte[] byteData) {
		return Base64.getEncoder().encodeToString(byteData);
	}

	protected ArrayList<String> listDir(String location) {
		ArrayList<String> dirs = new ArrayList<String>();
		File dir = new File(location);
		File[] filesList = dir.listFiles();
		for (File file : filesList) {
			dirs.add(file.getPath());
		}
		return dirs;
	}
	public class ClassBytes {
		public String myName;
		public byte[] myClassBytes;
		//public Test test = null;

		public ClassBytes(String name, String classData) {
			myName = name;
			myClassBytes = hexStringToByteArray(classData);
		}

		public ClassBytes(String name, byte[] classData) {
			myName = name;
			myClassBytes = classData.clone();
		}

		public byte[] hexStringToByteArray(String s) {
			int len = s.length();
			byte[] data = new byte[len / 2];
			for (int i = 0; i < len; i += 2) {
				data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character
						.digit(s.charAt(i + 1), 16));
			}
			return data;
		}
	}

	public class ByteClassLoader extends ClassLoader {
		private final Map<String, byte[]> definitions;

	    public ByteClassLoader(JarFile jar, Map<String, byte[]> definitions) throws MalformedURLException {
	        super(new URLClassLoader(new URL[] { new URL("jar:" + new File(jar.getName()).toURI().toURL() + "!/") }));
	        this.definitions = definitions;
	    }

		public ByteClassLoader() {
			definitions = new HashMap<String, byte[]>();
		}

		public void loadThisClass(String name, byte[] bytes) {
			loadThisClass(new ClassBytes(name, bytes));
		}

		public void loadThisClass(ClassBytes classByte) {
			
			if (definitions.containsKey(classByte.myName)) {
				// TODO create new class loader?
				return;
			}
			resolveClass(defineClass(classByte.myName, classByte.myClassBytes,
					0, classByte.myClassBytes.length));
			definitions.put(classByte.myName, classByte.myClassBytes);
		}
		public boolean containsClass(ClassBytes classBytes) {
		  return definitions.containsKey(classBytes.myName);
		}
		public boolean containsClassIsEqual(ClassBytes classBytes) {
			boolean all_match = definitions.containsKey(classBytes.myName)  && 
					definitions.get(classBytes.myName).length == (classBytes.myClassBytes).length;
			if (all_match) {
				byte[] _cbytes = definitions.get(classBytes.myName);
				for (int i = 0; i < _cbytes.length; i++) {
					all_match = _cbytes[i] == classBytes.myClassBytes[i];
					if (!all_match) break;
				}
			}
			return all_match;
		}
		public Map<String, byte[]> getDefinitions(){
			return this.definitions;
		}
		public void updateDefinitions(Map<String, byte[]> definitions){
			for (String k : definitions.keySet()) {
				if (!this.definitions.containsKey(k)) {
					loadThisClass(new ClassBytes(k, definitions.get(k)));
				}
			}
		}
	}

	private ByteClassLoader myBCL;
	private Object dummy = new Object();
	private HashMap<Integer, Process> execProcess = new HashMap<Integer, Process>();
	private String myConnectHost;
	private int myConnectHostPort;

	public Loader() {
		myBCL = new ByteClassLoader();
	}
	@SuppressWarnings("unchecked")
	public void classLoader(String className, byte [] classData) {
		ClassBytes cb = new ClassBytes(className, classData);
		classLoader(cb);
	}
	@SuppressWarnings("unchecked")
	public void classLoader(String className, String classData) {
		ClassBytes cb = new ClassBytes(className, classData);
		classLoader(cb);
	}
	
	@SuppressWarnings("unchecked")
	public void classLoader(ClassBytes cb) {
		//System.out.println(cb.myName);
		if (!myBCL.containsClass(cb)){
			myBCL.loadThisClass(cb);
		} else if (!myBCL.containsClassIsEqual(cb)){
			Map<String, byte[]> defs = myBCL.getDefinitions();
			defs.remove(cb.myName);
			myBCL = new ByteClassLoader();
			myBCL.updateDefinitions(defs);
			myBCL.loadThisClass(cb);
		}
	}

	public CommandResult call_method(String className, String methodName, Object[] params) {
		try {
			Object klass = (myBCL.loadClass(className)).newInstance();
			Object[] cap_parameters ={params};
			Method method = klass.getClass().getMethod(methodName, Object[].class);

			try {
				return new CommandResult( method.invoke(klass, cap_parameters), false);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return new CommandResult();
	}

	public CommandResult call_method_handler(String className, String methodName, ClientCommandHandler cch, String params) {
		try {
			Object klass = (myBCL.loadClass(className)).newInstance();
			Method method = klass.getClass().getMethod(methodName, Loader.class, ClientCommandHandler.class, String.class);
			try {
				return new CommandResult(method.invoke(klass, Loader.getLoaderInstance(), cch, params), false);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return new CommandResult();
	}
	public CommandResult call_method_handler_static(String className, String methodName, ClientCommandHandler cch, String params) {
		try {
			Class<?> klass = (myBCL.loadClass(className));
			Method method = klass.getMethod(methodName, Loader.class, ClientCommandHandler.class, String.class);
			try {
				return new CommandResult( method.invoke(klass, Loader.getLoaderInstance(), cch, params), false);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return new CommandResult();
	}
	protected int writeData(String location, byte[] data) throws IOException {
		File f = new File(location);
		if (f.exists() && f.isFile()) {
			Files.write(f.toPath(), data, StandardOpenOption.TRUNCATE_EXISTING);
			return data.length;
		}else if (!f.isDirectory()) {
			Files.write(f.toPath(), data, StandardOpenOption.CREATE);
			return data.length;
		}
		return 0;
	}
	
	protected byte[] readData(String location) throws IOException {
		File f = new File(location);
		if (f.exists() && f.isFile()) {
			return Files.readAllBytes(f.toPath());
		}
		return new byte[0];
	}
	protected int appendData(String location, byte[] data) throws IOException {
		File f = new File(location);
		int len = 0;
		if (f.exists() && f.isFile()) {
			Files.write(f.toPath(), data, StandardOpenOption.APPEND);
			len = data.length;
		}else if (!f.isDirectory()) {
			Files.write(f.toPath(), data, StandardOpenOption.CREATE);
			len = data.length;
		}
		return len;
		
	}

	protected byte [] downloadFile(String urlString) {
        byte [] result = null;
		try {
            // get URL content
			ArrayList<Byte[]> buffers = new ArrayList<Byte[]>();
            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();
            byte [] buffer = new byte[8096];
            // open the stream and put it into BufferedReader
            InputStream inStream = conn.getInputStream();
            int n = 0;
            int totalSize = 0;
            while ( (n = inStream.read(buffer)) != -1)
            {	
            	Byte [] buf = new Byte[n];
            	for (int i = 0; i < n; i++)
            		buf[i] = buffer[i];
            	totalSize += n;
                buffers.add(buf);
            }
            inStream.close();
            result = new byte[totalSize];
            n = 0;
            for (Byte [] buf: buffers) {
            	for (int i = 0; i < buf.length; i++){
            		result[n+i] = buf[i];
            	}
            	n += buf.length;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
		return result;
		
	}

	protected boolean closeSocket(String host, int port) throws IOException {
		return closeSocket(host, port, "tcp");
	}
	protected boolean closeSocket(String host, int port, String proto) throws IOException {
		String inetAddr = (InetAddress.getByName(host)).getHostAddress();
		String hostKey = String.format("%s:%s:%d", proto, inetAddr, port);
		if (!mySocketMap.containsKey(hostKey)) return false;
		Socket sock = mySocketMap.get(hostKey).myClientSocket;
		sock.close();
		mySocketMap.remove(hostKey);
		return true;
	}
	
	protected boolean openSocket(String host, int port) throws UnknownHostException, IOException {
		return openSocket(host, port, "tcp");
	}
	protected boolean openSocket(String host, int port, String proto) throws UnknownHostException, IOException {
		Socket sock = new Socket(host, port);
		boolean success = false;
		if (sock != null){
			String ipAddr = sock.getInetAddress().toString();
			int rPort = sock.getPort();
			String hostKey = String.format("%s:%s:%d", proto, ipAddr, rPort);	
			mySocketMap.put(hostKey, new ClientCommandHandler(sock));
			success = true;
		}
		return success;
			
	}
	
	protected int sendSocketData(int id, String host, int port, byte [] data) throws IOException {
		return sendSocketData(id, host, port, "tcp", data);
	}
	protected int sendSocketData(int id, String host, int port, String proto, byte [] data) throws IOException {
		int len = -1;
		String inetAddr = (InetAddress.getByName(host)).getHostAddress();
		String hostKey = String.format("%s:%s:%d", proto, inetAddr, port);
		if (!mySocketMap.containsKey(hostKey)) return len;
		Socket sock = mySocketMap.get(hostKey).myClientSocket;
		try {
			BufferedOutputStream bos = new BufferedOutputStream(sock.getOutputStream());
			(new DataOutputStream(sock.getOutputStream())).writeInt(data.length);
			(new DataOutputStream(sock.getOutputStream())).writeInt(id);			
			len = 8;
			bos.write(data);
			len += data.length;
			bos.flush();
		} catch (IOException e) {
		}
		return len;
	}
	protected int sendSocketRawData(int id, String host, int port, byte [] data) throws IOException {
		return sendSocketRawData(id, host, port, "tcp", data);
	}
	protected int sendSocketRawData(int id, String host, int port, String proto, byte [] data) throws IOException {
		int len = -1;
		String inetAddr = (InetAddress.getByName(host)).getHostAddress();
		String hostKey = String.format("%s:%s:%d", proto, inetAddr, port);
		if (!mySocketMap.containsKey(hostKey)) return len;
		Socket sock = mySocketMap.get(hostKey).myClientSocket;
		try {
			BufferedOutputStream bos = new BufferedOutputStream(sock.getOutputStream());
			bos.write(data);
			len = data.length;
			bos.flush();
		} catch (IOException e) {
		}
		return len;
	}
	protected byte [] recvSocketData(String host, int port, int length) throws IOException {
		return recvSocketData(host, port, "tcp", length);
	}
	protected byte [] recvSocketData(String host, int port, String proto, int length) throws IOException {
		String inetAddr = (InetAddress.getByName(host)).getHostAddress();
		String hostKey = String.format("%s:%s:%d", proto, inetAddr, port);
		if (!mySocketMap.containsKey(hostKey)) return new byte[0];
		Socket sock = mySocketMap.get(hostKey).myClientSocket;
		BufferedInputStream bos = new BufferedInputStream(sock.getInputStream());
		int len = length < 1 ? bos.available() : length;
		byte [] data = new byte[len];
		if (data.length == 0)
			return data;
		try {
			int actLen = bos.read(data);
			if (actLen < 1)
				actLen = 0;
			byte [] t = new byte[actLen];
			for (int i =0; i < t.length; i++)
				t[i] = data[i];
			data = t;
		} catch (IOException e) {
			data = new byte[0];
		}
		return data;
	}

	public void addClientHandlerSocket(ClientCommandHandler cch) {
		String hostKey = String.format("%s:%s:%d", "tcp", cch.myIpAddr, cch.myPort);
		//if (mySocketMap.containsKey(hostKey)) return;
		mySocketMap.put(hostKey, cch);
	}
	
	public void stdout(String msg) {
		synchronized (dummy) {
			System.out.println(msg);
		}
	}
	
	public void logEvent(String action, String command, int sz, int eventId, String addl) {
		long time = System.currentTimeMillis();
		if (addl == null || addl.length() == 0) {
			addl = "NONE";
		}
		String out = String
				.format("Time:%08x-Action:%s-Command:%s-Size:%08x-EventId:%08x-Details:%s",
					time, action, command, sz, eventId, addl);
		Loader.getLoaderInstance().stdout(out);
	}
	
	public static void main(String[] args) {
		
		int listenPort = args.length > 0 ? Integer.parseInt(args[0]) : DEFAULT_PORT;
		int runtimeSecs = args.length > 1 ? Integer.parseInt(args[1]) : DEFAULT_RUNTIME_SECONDS;
		String connectHost = args.length > 2 ? args[2]: "";
		int connectPort = args.length > 3 ? Integer.parseInt(args[3]) : DEFAULT_PORT;
		Loader ldr = Loader.getLoaderInstance();
		ldr.myServerPort = listenPort;
		if (connectHost.length() > 0){
			ldr.myConnectHost = connectHost;
			ldr.myConnectHostPort = connectPort;
		}
		
		
//		boolean testit = false;
//		if (testit) {
//			ldr.classLoader("Test2", example2);
//			ldr.classLoader("Test", example);
//			Object []x = new Object[3];
//			ldr.call_method("Test2", "doit", x);
//			ldr.call_method_handler("Test2", "doit", null, null);
//		}
		Loader.getLoaderInstance().logEvent("serverStart", "NONE", -1, -1, Integer.toString(listenPort));
		Loader.getLoaderInstance().logEvent("runTime", "NONE", -1, -1, Integer.toString(runtimeSecs));
		Loader.getLoaderInstance().start();
		try {
			Thread.sleep((runtimeSecs*1000));
		} catch (InterruptedException e1) {
			e1.printStackTrace();
			ldr.myRunServerSocket = false;
			ldr.serverTask.interrupt();
		}
		

	}

	public int writeProcessStdin(int pid, byte[] sData) {
		Process p = null;
		if (! execProcess.containsKey(pid)) {
			return -1;
		}
		p = execProcess.get(pid);
		if (p.isAlive()) {
		    try {
				p.getOutputStream().write(sData);
				return sData.length;
		    } catch (IOException e) {
		    	return -1;
		    }		    
		}
		return 0;
	}
	public static int getPid(Process process) {
	    try {
	        Class<?> cProcessImpl = process.getClass();
	        java.lang.reflect.Field fPid = cProcessImpl.getDeclaredField("pid");
	        if (!fPid.isAccessible()) {
	            fPid.setAccessible(true);
	        }
	        return fPid.getInt(process);
	    } catch (Exception e) {
	        return -1;
	    }
	}
	public int startProcess(String command, String command_args) {
		StringTokenizer strTok = new StringTokenizer(command_args);
		String [] tokenized = new String[strTok.countTokens()+1];
		int pos = 0;
		tokenized[pos] = command;
		pos +=1;
		while (strTok.hasMoreTokens()) {
			tokenized[pos] = strTok.nextToken();
			pos +=1;
		}
		
		ProcessBuilder pb = new ProcessBuilder(tokenized);
		
		int pid = -1;
		try {
			Process p = pb.start();
			pid = getPid(p);
			if (pid != -1)
				execProcess.put(pid, p);
		} catch (IOException e) {
		}
		return pid;
	}

	public int killProcess(int pid) {
		if (execProcess.containsKey(pid)) {
			Process p = execProcess.get(pid);
			execProcess.remove(p);
			p.destroyForcibly();
			return p.exitValue();
		}
		return -1;
	}

	public String readProcessStdout(int pid) {
		Process p = null;
	    byte [] data = new byte[0];
		if (! execProcess.containsKey(pid)) {
			return Base64.getEncoder().encodeToString(data);
		}
		p = execProcess.get(pid);
		int len = -1;
		try {
			len = p.getInputStream().available();
		} catch (IOException e1) {
		}
		if (len > 0) {

			try {
				if (len > 0) {
					data = new byte[len];
					p.getInputStream().read(data);
				}
				return Base64.getEncoder().encodeToString(data);
		    } catch (IOException e) {
		    }		    
		}
		return Base64.getEncoder().encodeToString(data);
	}

	public String readProcessStderr(int pid) {
		Process p = null;
	    byte [] data = new byte[0];
		if (! execProcess.containsKey(pid)) {
			return Base64.getEncoder().encodeToString(data);
		}
		p = execProcess.get(pid);
		int len = -1;
		try {
			len = p.getErrorStream().available();
		} catch (IOException e1) {
		}
		if (len > 0) {
			try {
				len = p.getErrorStream().available();
				if (len > 0) {
					data = new byte[len];
					p.getErrorStream().read(data);
				}
				return Base64.getEncoder().encodeToString(data);
		    } catch (IOException e) {
		    }		    
		}
		return Base64.getEncoder().encodeToString(data);
	}
}
