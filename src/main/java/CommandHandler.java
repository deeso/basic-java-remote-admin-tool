import java.util.ArrayList;
import java.util.HashMap;


public interface CommandHandler {
	HashMap<String, Command> mySupportedCommands = new HashMap<String, Command>();
	//void updateCommandParameters(String parameter, Object value);
	
	ArrayList<Command> initializeCommands(CommandHandler cmdHandler);

	void addCommands(ArrayList<Command> initializeCommands);
	String getCommandsHelpString();
	
}	
