import java.io.IOException;
import java.util.StringTokenizer;


public abstract class Command {
	String myName;
	String myDescription;
	int myParams;
	CommandHandler myCommandHandler;
	
	public Command(CommandHandler cmdHandler, String name, int params) {
		myCommandHandler = cmdHandler;
		myName = name;
		myParams = params;
		myDescription = "";
	}
	
	public Command(CommandHandler cmdHandler, String name, int params, String description) {
		myCommandHandler = cmdHandler;
		myName = name;
		myParams = params;
		myDescription = description;
	}
	public abstract void do_it(int id, String data) throws IOException, InstantiationException, IllegalAccessException;
	
	public String helpString() {
		String nameString = String.format("\"name\":\"%s\"", myName);
		String paramString = String.format("\"parameters\":\"%s\"", myParams);
		String descString = String.format("\"decription\":\"%s\"", myDescription);
		if (myDescription.length() == 0)
		    descString = String.format("\"decription\":\"%s\"", myName);
		return String.format("{%s, %s, %s}", nameString, paramString, descString);
	}
	public String [] getParams(String data){
		String [] results = null;
		StringTokenizer st = new StringTokenizer(data, ",");
		if (st.countTokens() >= myParams) {
			results = new String[st.countTokens()];
			int pos = 0;
			while (st.hasMoreTokens()){
				results[pos] = st.nextToken();
				pos++;					
			}
		}
		return results;
	}
	
}