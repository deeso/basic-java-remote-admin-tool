
public class Test2 {
	static public Test test = Test.getInstance();
	static public final Test2 test2 = new Test2();
	private Test2 ( ) {
		System.out.println("Test2 v3 loaded successfully");
	}
	
	@SuppressWarnings("unused")
	private static Test2 getInstance() {
		return test2;
	}


	public static void doit(Object[] params) {
		Test.doit(params);
		System.out.println("Test2.doit v2 called");
	}
	public static void doit(Loader loader, ClientCommandHandler clientCommandHandler, String params) {
		Test.doit(loader, clientCommandHandler, params);
		System.out.println("Test2.doit v2 called");
	}
	
	public static void main(String[] args) {
		Test.doit(null, null, null);
	}
}
