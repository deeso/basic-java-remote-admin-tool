
public class Test {
	static final public Test test = new Test();
	
	private Test ( ) {
		System.out.println("Test v3 loaded successfully");
	}
	
	static Test getInstance() {
		return test;
	}
	public static void doit(Loader loader, ClientCommandHandler clientCommandHandler, String params) {
		
		
		System.out.println("Test.doit called");
	}
	
	public static void doit(Object [] params) {
		System.out.println("Test.doit called");
	}
	
	public static void main(String[] args) {
		Object [] obj = new Object[3];
				
		Test.doit(obj);
	}
}
